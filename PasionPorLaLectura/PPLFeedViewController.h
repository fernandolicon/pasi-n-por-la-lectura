//
//  PPLFeedViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLFeed.h"
#import "PPLReviewTableViewCell.h"
#import "PPLFeedRequest.h"
#import "PPLUser.h"
#import "ThirdClasses/TPCRateView.h"

@interface PPLFeedViewController : UITableViewController{
    NSMutableArray *feeds;
    UIApplication *networkIdentifier;
    UIRefreshControl *refreshFeed;
    BOOL reload;
    int selectedRow;
    UIView *whiteView;
    
    PPLBook *selectedBook;
    PPLUser *selectedUser;
    
    NSMutableDictionary *imagesFeed;
    
    BOOL emptyFeed;
}

- (void) errorInConnection;
- (void) successfulConnectionwithArray: (NSMutableArray *) listFeeds;

@end
