//
//  PPLMyReviewTableViewCell.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 12/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPCRateView.h"

@interface PPLMyReviewTableViewCell : UITableViewCell<RateViewDelegate>

@property (weak, nonatomic) IBOutlet TPCRateView *ownReview;

@end
