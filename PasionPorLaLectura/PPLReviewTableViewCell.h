//
//  ReviewTableViewCell.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPCRateView.h"

@interface PPLReviewTableViewCell : UITableViewCell<RateViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *txtCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UIButton *bttnSeeReview;
@property (weak, nonatomic) IBOutlet TPCRateView *rateView;


@end
