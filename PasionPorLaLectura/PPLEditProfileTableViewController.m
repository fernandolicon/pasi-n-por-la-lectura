//
//  EditProfileTableViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLEditProfileTableViewController.h"

@interface PPLEditProfileTableViewController ()

@end

@implementation PPLEditProfileTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    editPhotoPopUp = [[UIActionSheet alloc] init];
    
     editPhotoPopUp = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [editPhotoPopUp addButtonWithTitle:NSLocalizedString(@"Tomar Foto", nil)];
    }
    
    [editPhotoPopUp addButtonWithTitle:NSLocalizedString(@"Elegir de Biblioteca", nil)];
    [editPhotoPopUp addButtonWithTitle:@"Cancelar"];
    editPhotoPopUp.cancelButtonIndex = editPhotoPopUp.numberOfButtons-1;
    
    networkIndicator = [UIApplication sharedApplication];
    
    self.title = @"Editar perfil";
    
    pictureUpdated = NO;
    quoteUpdated = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    if (quoteUpdated) {
        [self.delegate writeNewQuote:_quote];
    }
    
    if (pictureUpdated){
        [self.delegate changeProfilePicture:imageCropped];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        PPLEditProfilePhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditPhotoCell"];
        
        if (cell == nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLEditProfilePhotoTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.imgProfile.image = _imgUser;
        
        return cell;
        
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditProfileCell"];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"EditProfileCell"];
        }
        
        cell.textLabel.text = @"Cambiar cita";
        cell.textLabel.textColor = [UIColor colorWithRed:(85/255.f) green:(85/255.f) blue:(85/255.f) alpha:1];
        
        return cell;
    }
    
    //It would never get to this poinr
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 102;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            [editPhotoPopUp showInView:[UIApplication sharedApplication].keyWindow];
            break;
        case 1:
            [self performSegueWithIdentifier:@"EditarCita" sender:self];
            break;
        default:
            break;
    }
}

#pragma mark - UIActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Elegir de Biblioteca", nil)]) {
        [self openPhotoAlbum];
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"Tomar Foto", nil)]) {
        [self showCamera];
    }
}

#pragma mark - Photo Methods

- (void)openPhotoAlbum{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (popController.isPopoverVisible) {
            [popController dismissPopoverAnimated:NO];
        }
        
        popController = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popController presentPopoverFromBarButtonItem:nil
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
    } else {
        [self presentViewController:controller animated:YES completion:NULL];
    }
}


- (void)showCamera
{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (popController.isPopoverVisible) {
            [popController dismissPopoverAnimated:NO];
        }
        
        popController = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popController presentPopoverFromBarButtonItem:nil
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
    } else {
        [self presentViewController:controller animated:YES completion:NULL];
    }
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    networkIndicator.networkActivityIndicatorVisible = YES;
    imageCropped = croppedImage;
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [PPLUpdateProfileRequest updateProfielPicture:croppedImage forView:self];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller{
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UIImagePickerControllerDelegate methods

/*
 Open PECropViewController automattically when image selected.
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    imageSelected = image;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (popController.isPopoverVisible) {
            [popController dismissPopoverAnimated:NO];
        }
        
        [self openEditor];
    } else {
        [picker dismissViewControllerAnimated:YES completion:^{
            [self openEditor];
        }];
    }
}

#pragma mark - Editor methods

- (void)openEditor
{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = imageSelected;
    
    UIImage *image = imageSelected;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length,
                                          length);
    controller.keepingCropAspectRatio = YES;
    controller.cropAspectRatio = 1.0;
    [controller setToolbarHidden:YES];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"EditarCita"]) {
        PPLEditCitaViewController *controller = [segue destinationViewController];
        controller.quote = _quote;
        controller.delegate = self;
    }
}

#pragma mark - Network Responses

- (void)successfulUpdate{
    imageSelected = nil;
    networkIndicator.networkActivityIndicatorVisible = NO;
    PPLEditProfilePhotoTableViewCell *cell = [self.tableView.visibleCells objectAtIndex:0];
    cell.imgProfile.image = imageCropped;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡Hecho!" message:@"La foto ha sido actualizada" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    pictureUpdated = YES;
}

- (void) errorInConnectionforPicture{
    imageCropped = nil;
    imageSelected = nil;
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión. Intente más tarde." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

#pragma mark - Delegate
- (void) getNewQuote:(NSString *)newQuote{
    _quote = newQuote;
    quoteUpdated = YES;
}

@end
