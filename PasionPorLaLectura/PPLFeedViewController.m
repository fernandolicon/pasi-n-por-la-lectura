//
//  PPLFeedViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLFeedViewController.h"
#import "PPLViewReviewViewController.h"
#import "PPLAppDelegate.h"
#import "SVProgressHUD.h"

@interface PPLFeedViewController ()

@end

@implementation PPLFeedViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.title = @"News Feed";
    
    reload = NO;
    
    networkIdentifier = [UIApplication sharedApplication];
    networkIdentifier.networkActivityIndicatorVisible = YES;
    
    refreshFeed = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:refreshFeed];
    [refreshFeed addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    imagesFeed = [[NSMutableDictionary alloc] init];
    
    emptyFeed = NO;
    
    //Don't present data until all conections were performed
    whiteView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [whiteView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:whiteView];
    [self.view bringSubviewToFront:whiteView];
    
    [SVProgressHUD show];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self performConnection];
}

- (void) viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (feeds.count == 0) {
        emptyFeed = YES;
        return 1;
    }else{
    emptyFeed = NO;
    return feeds.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (emptyFeed) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCell"];
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"feedCell"];
        }
        
        cell.textLabel.text = @"No se encontraron nuevas noticias";
        return cell;
    }
    
    PPLReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bookCell"];
    PPLFeed *provisionalFeed = [feeds objectAtIndex:indexPath.row];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLReviewTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.txtCell.text = [NSString stringWithFormat:@"%@ ha hecho una reseña de %@", provisionalFeed.userFeed.firstName, provisionalFeed.bookFeed.name];
    cell.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
    cell.rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
    cell.rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    cell.rateView.rating = 0;
    cell.rateView.maxRating = 5;
    cell.rateView.delegate = cell;
    cell.rateView.editable = NO;
    
    //http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=luis&pwd=1&photo_from_user_id=1&width=100
    NSString *key = [NSString stringWithFormat:@"%i", provisionalFeed.userFeed.idUser];
    cell.imageView.image = [UIImage imageWithData:[imagesFeed objectForKey:key]];
    
    cell.rateView.rating = provisionalFeed.rate;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!emptyFeed) {
        selectedRow = (int)indexPath.row;
        [self performSegueWithIdentifier:@"ViewReviewFromFeed" sender:self];
    }
}

#pragma mark - Refresh

- (void) refreshTable{
    reload = YES;
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [self performConnection];
}

#pragma mark - NetworkConnections

- (void) performConnection{
    [PPLFeedRequest getFeedforViewController:self];
}

- (void) successfulConnectionwithArray:(NSMutableArray *) listFeeds{
    whiteView.hidden = YES;
    [SVProgressHUD dismiss];
    
    networkIdentifier.networkActivityIndicatorVisible = NO;
    feeds = listFeeds;
    
    if (reload) {
        [refreshFeed endRefreshing];
        reload = NO;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        [self.tableView reloadData];
    }
    
    //This for downloads images for the feed in order to improve performance
    for (PPLFeed *feed in listFeeds){
        NSString *key = [NSString stringWithFormat:@"%i", feed.userFeed.idUser];
        if (!([imagesFeed objectForKey:key])) {
            NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=70", [PPLUser getUserName], [PPLUser getPassword], feed.userFeed.idUser];
            NSURL *url = [[NSURL alloc] initWithString:urlString];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            [imagesFeed setObject:imageData forKey:key];
        }
    }
}

- (void) errorInConnection{
    whiteView.hidden = YES;
    [SVProgressHUD dismiss];
    
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ViewReviewFromFeed"]) {
        PPLViewReviewViewController *nextVC = [segue destinationViewController];
        PPLFeed *nextFeed = [feeds objectAtIndex:selectedRow];
        NSString *key = [NSString stringWithFormat:@"%i", nextFeed.userFeed.idUser];
        nextVC.imageUser = [imagesFeed objectForKey:key];
        nextVC.nameBook = nextFeed.bookFeed.name;
        nextVC.authorBook = nextFeed.bookFeed.author;
        PPLReview *review = [[PPLReview alloc] init];
        review.reviewText = nextFeed.reviewText;
        review.rate = nextFeed.rate;
        review.user = nextFeed.userFeed;
        nextVC.review = review;
    }
}

@end
