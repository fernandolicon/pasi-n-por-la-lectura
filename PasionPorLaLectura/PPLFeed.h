//
//  PPLFeed.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLUser.h"
#import "PPLBook.h"

@interface PPLFeed : NSObject

@property (nonatomic, strong) PPLUser *userFeed;
@property (nonatomic, strong) PPLBook *bookFeed;
@property (nonatomic, strong) NSString *reviewText;
@property int rate;

- (instancetype) initWithDictionary:(NSDictionary *)feedDictionary;

@end
