//
//  ProfileActiveUserViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 27/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUser.h"
#import "PPLUserInfo.h"
#import "PPLEditProfileTableViewController.h"
#import "PPLFollowersViewController.h"
#import "PPLListBooksViewController.h"

@interface PPLProfileActiveUserViewController : UIViewController <EditPerfilDelegate>{
    __weak IBOutlet UIImageView *imgPerfil;
    __weak IBOutlet UITextView *txtCita;
    __weak IBOutlet UITextView *txtNombre;
    __weak IBOutlet UILabel *txtPrograma;
    __weak IBOutlet UILabel *txtSeguidores;
    __weak IBOutlet UILabel *txtSiguiendo;
    __weak IBOutlet UILabel *txtLeidos;
    __weak IBOutlet UIScrollView *scrllView;
    
    int connectionsCounter;
    BOOL reload;
    
    UIApplication *networkIndicator;
    UIRefreshControl *refreshUser;
    NSMutableArray *listUsrFollowing;
    NSMutableArray *listUsrFollowers;
    NSMutableArray *listReadBooks;
    NSMutableArray *sentUsers; //Used to storage followers or following depending on what is called
    
    NSString *nextViewTitle;
    
    PPLUser *activeUsr;
}

@property (nonatomic, strong) NSString *updatedQuote;

- (void) successfulConnectionforUser: (PPLUser *) activeUser;
- (void) errorinConnection;
- (void) successfulConnectionforFollowingUsers: (NSMutableArray *) usersFollowing;
- (void) successfulConnectionforFollowdUsers: (NSMutableArray *) usersFollowed;
- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead;
- (IBAction)editProfile:(id)sender;

- (IBAction)viewFollowers:(id)sender;
- (IBAction)viewFollowing:(id)sender;
- (IBAction)viewReadBooks:(id)sender;

@end
