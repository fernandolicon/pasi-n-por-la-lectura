//
//  RateView.h
//  pasionporlectura
//
//  Created by ricardo on 12/3/13.
//  Copyright (c) 2013 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPCRateView;

@protocol RateViewDelegate
- (void)rateView:(TPCRateView *)rateView ratingDidChange:(float)rating;
@end

@interface TPCRateView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <RateViewDelegate> delegate;

@end
