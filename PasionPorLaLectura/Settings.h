//
//  Settings.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#ifndef PasionPorLaLectura_Settings_h
#define PasionPorLaLectura_Settings_h

//This is the base url for the Web Services requests
#define pasionWSBaseUrl @"http://cml.itesm.mx:8080/Passion/webresources/"

#endif

//Problems with this file