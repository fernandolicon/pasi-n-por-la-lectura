//
//  UpdateQuoteRequest.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 31/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Settings.h"
#import "AFHTTPRequestOperationManager.h"
#import "PPLUser.h"
@class PPLEditProfileTableViewController;
@class PPLEditCitaViewController;

@interface PPLUpdateProfileRequest : NSObject

+ (void) updateQuote: (NSString *) quoteEncoded forView: (PPLEditCitaViewController *) controller;
+ (void) updateProfielPicture: (UIImage *) newImage forView: (PPLEditProfileTableViewController *) controller;

@end
