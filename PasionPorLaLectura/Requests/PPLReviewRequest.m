//
//  ReviewRequest.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLReviewRequest.h"
#import "PPLAllReviewsTableViewController.h"

@implementation PPLReviewRequest

+ (void) getReviewforEndorsement:(PPLEndorsement *)endorsement andView:(PPLSeeEndorsementViewController *)controller{
    NSString *userName = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@Book/getReviews?pattern=%@&pwd=%@&id_book=%i&user_filter=%i", pasionWSBaseUrl, userName, psswd, endorsement.book.idBook, endorsement.user.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        PPLReview *review = [[PPLReview alloc] initWithDictionary:[diccProvisional objectForKey:@"bookreview-0"]];
        
        [controller successfulConnectionwithReview:review];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
    
}

+ (void) getLastReviewforIdBook:(int)idBook andView:(PPLBookViewController *)controller{
    NSString *userName = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    int idUser = [PPLUser getIdUser];
    
    NSString *url = [NSString stringWithFormat:@"%@Book/getReviews?pattern=%@&pwd=%@&id_book=%i&user_filter=%i", pasionWSBaseUrl, userName, psswd,idBook, idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSArray *objects = [diccProvisional objectForKey:@"objs"];
        if (objects.count != 0) {
            PPLReview *review = [[PPLReview alloc] initWithDictionary:[diccProvisional objectForKey:@"bookreview-0"]];
            [controller successfulConnectionwithReview:review];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

+ (void) setReview:(NSString *)review withRate:(int)rate forBook:(int)idBook andView:(PPLReviewViewController *)controller{
    NSString *userName = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@User/addBook?pattern=%@&pwd=%@&id_book=%i&rate=%i&review=%@", pasionWSBaseUrl, userName, psswd,idBook, rate, review];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        if ( [[diccProvisional objectForKey:@"status" ] isEqualToString:@"OK"] ) {
            [controller successfulConnection];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

+ (void) getLastReviewsforBook: (int) idBook fromReviewNumber: (int) from numberofReviews: (int) numberReviews forView: (PPLBookViewController *) controller{
    NSString *userName = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *reviews = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@Book/getReviews?pattern=%@&pwd=%@&id_book=%i&limit_from=%i&limit_count=%i", pasionWSBaseUrl, userName, psswd, idBook, from, numberReviews];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *namesObjects = [diccProvisional objectForKey:@"objs"];
        
        for (int i = 0; i < namesObjects.count; i++) {
            NSString *numberofEndorse = namesObjects[i];
            PPLReview *provisionalReview = [[PPLReview alloc] initWithDictionary:[diccProvisional objectForKey:numberofEndorse]];
            [reviews addObject:provisionalReview];
        }
        [controller successfulConnectionwithLastReviews:reviews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];

}

+ (void) getAllLastReviewsforBook: (int) idBook fromReviewNumber: (int) from numberofReviews: (int) numberReviews forView: (PPLAllReviewsTableViewController *) controller{
    NSString *userName = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *reviews = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@Book/getReviews?pattern=%@&pwd=%@&id_book=%i&limit_from=%i&limit_count=%i", pasionWSBaseUrl, userName, psswd, idBook, from, numberReviews];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *namesObjects = [diccProvisional objectForKey:@"objs"];
        int totalReviews;
        
        for (int i = 0; i < namesObjects.count; i++) {
            NSString *numberofEndorse = namesObjects[i];
            PPLReview *provisionalReview = [[PPLReview alloc] initWithDictionary:[diccProvisional objectForKey:numberofEndorse]];
            totalReviews = [[[diccProvisional objectForKey:numberofEndorse] objectForKey:@"total_reviews"] intValue];
            [reviews addObject:provisionalReview];
        }
        [controller successfulConnectionwithArray:reviews andTotalReviews:totalReviews];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
    
}

@end
