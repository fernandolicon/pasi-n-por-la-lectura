//
//  EndorsmentsRequest.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "PPLEndorsement.h"
#import "Settings.h"
#import "PPLPresentEndorsementsViewController.h"
#import "PPLAskEndorsementViewController.h"
@class PPLPresentEndorsementsViewController;
@class PPLSeeEndorsementViewController;
@class PPLAskEndorsementViewController;

@interface PPLEndorsementsRequest : NSObject

+ (void) getEndorsmentswithStatus: (NSString *) status forViewController: (PPLPresentEndorsementsViewController *) controller;

+ (void) endorseBookforEndorsement: (PPLEndorsement *) endorse forViewController: (PPLSeeEndorsementViewController *) controller;

+ (void) askEndorsementToTeacher: (int) idTeacher forBook: (int) idBook andView: (PPLAskEndorsementViewController *) controller;

@end
