//
//  BooksReqests.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "PPLUser.h"
#import "PPLBook.h"
#import "PPLSearchBookViewController.h"
#import "PPLMyBooksViewController.h"
#import "Settings.h"
@class PPLSearchBookViewController;
@class PPLMyBooksViewController;

@interface PPLBooksReqests : NSObject

+ (void) searchBookwithString: (NSString *) searchString andView: (PPLSearchBookViewController *) controller;
+ (void) getBooksReadedbyActiveUserforView: (PPLMyBooksViewController *) controller;

@end
