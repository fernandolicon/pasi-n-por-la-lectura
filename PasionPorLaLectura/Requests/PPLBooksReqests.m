//
//  BooksReqests.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLBooksReqests.h"

@implementation PPLBooksReqests

+ (void) searchBookwithString: (NSString *) searchString andView:(PPLSearchBookViewController *)controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *booksFounded = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@Book/search?pattern=%@&pwd=%@&any=%@", pasionWSBaseUrl, user, psswd, searchString];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalBooks = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalBooks.count; i++) {
            NSString *bookCalled = provisionalBooks[i];
            PPLBook *provisionalBook = [[PPLBook alloc] initWithDictionary:[diccProvisional objectForKey:bookCalled]];
            [booksFounded addObject:provisionalBook];
        }
        [controller successfulSearchofBooks:booksFounded];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

+ (void) getBooksReadedbyActiveUserforView: (PPLMyBooksViewController *) controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *booksRead = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/getReviewedBooks?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalBooks = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalBooks.count; i++) {
            NSString *bookCalled = provisionalBooks[i];
            NSDictionary *diccBook = [diccProvisional objectForKey:bookCalled];
            PPLBook *provisionalBook = [[PPLBook alloc] initWithDictionary:[diccBook objectForKey:@"book"]];
            [booksRead addObject:provisionalBook];
        }
        [controller successfulConnectionforBooks:booksRead];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorinConnection];
    }];
}

@end
