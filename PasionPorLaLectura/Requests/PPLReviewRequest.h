//
//  ReviewRequest.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLReview.h"
#import "PPLEndorsement.h"
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"
#import "PPLSeeEndorsementViewController.h"
#import "PPLReviewViewController.h"
@class PPLSeeEndorsementViewController;
@class PPLReviewViewController;
@class PPLBookViewController;
@class PPLAllReviewsTableViewController;

@interface PPLReviewRequest : NSObject

+ (void) getReviewforEndorsement: (PPLEndorsement *) endorsement andView: (PPLSeeEndorsementViewController *) controller;
+ (void) getLastReviewforIdBook: (int) idBook andView: (PPLBookViewController *) controller;
+ (void) setReview: (NSString *) review withRate: (int) rate forBook: (int) idBook andView: (PPLReviewViewController *) controller;
+ (void) getLastReviewsforBook: (int) idBook fromReviewNumber: (int) from numberofReviews: (int) numberReviews forView: (PPLBookViewController *) controller;
+ (void) getAllLastReviewsforBook: (int) idBook fromReviewNumber: (int) from numberofReviews: (int) numberReviews forView: (PPLAllReviewsTableViewController *) controller;

@end
