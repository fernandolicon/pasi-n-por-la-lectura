//
//  ConnectUsersRequest.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLConnectUsersRequest.h"

@implementation PPLConnectUsersRequest

+ (void) checkIfActiveUserisFollowingUser:(PPLUser *)followedUser forView:(PPLOtherUserViewController *)controller{
    
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    int idUser = [PPLUser getIdUser];
    
    NSString *url = [NSString stringWithFormat:@"%@User/isUser1FollowingUser2?pattern=%@&pwd=%@&id_user1=%i&id_user2=%i", pasionWSBaseUrl, user, psswd, idUser, followedUser.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        
        if ([[diccionario objectForKey:@"status"] isEqualToString:@"Yes"]) {
            [controller userIsFollowing];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorinConnection];
    }];
}

+ (void) followUser: (PPLUser *) user forView: (PPLOtherUserViewController *) controller{
    NSString *usr = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@User/followUser?pattern=%@&pwd=%@&id_user_followed=%i", pasionWSBaseUrl, usr, psswd, user.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        
        if ([[diccionario objectForKey:@"status"] isEqualToString:@"OK"]) {
            [controller correctFollowUser];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorinConnection];
    }];
}

+ (void) unfollowUser: (PPLUser *) user forView: (PPLOtherUserViewController *) controller{
    NSString *usr = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@User/unfollowUser?pattern=%@&pwd=%@&id_user_followed=%i", pasionWSBaseUrl, usr, psswd, user.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        
        if ([[diccionario objectForKey:@"status"] isEqualToString:@"OK"]) {
            [controller correctUnfollowUser];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorinConnection];
    }];
}

@end
