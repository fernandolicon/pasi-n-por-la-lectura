//
//  UpdateQuoteRequest.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 31/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLUpdateProfileRequest.h"
#import "PPLEditCitaViewController.h"
#import "PPLEditProfileTableViewController.h"

@implementation PPLUpdateProfileRequest

+ (void) updateQuote: (NSString *) quoteEncoded forView: (PPLEditCitaViewController *) controller{
    
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSString *url = [NSString stringWithFormat:@"%@User/updateProfile?pattern=%@&pwd=%@&field=quote&value=%@", pasionWSBaseUrl, user, psswd, quoteEncoded];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager POST:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        if ([[diccionario objectForKey:@"valid"] isEqualToString:@"Yes"]){
            [controller successfulUpdate];
        }else{
            [controller errorInConnection];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
   
}

+ (void) updateProfielPicture:(UIImage *)newImage forView:(PPLEditProfileTableViewController *)controller{
    CGSize size = CGSizeMake(200.f, 200.f);
    UIGraphicsBeginImageContext(size);
    [newImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *dataForJPEGFile = UIImageJPEGRepresentation(destImage, 0.85);
    double compressionRatio = 1;
    int resizeAttempts = 5;
    
    //If the file is larger than 4
    while ([dataForJPEGFile length] > 25000 && resizeAttempts > 0) {
        resizeAttempts -= 1;
        //Increase the compression amount
        compressionRatio = compressionRatio*0.5;
        //Compression
        dataForJPEGFile = UIImageJPEGRepresentation(newImage,compressionRatio);
    }
    
    NSString *imageData = [dataForJPEGFile base64EncodedStringWithOptions:0];
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSString *url = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/UploadImage"];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    NSDictionary *parameters = @{@"pattern": user, @"pwd":psswd, @"picture":imageData};
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",nil];

    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        if ([[diccionario objectForKey:@"valid"] isEqualToString:@"Yes"]){
            [controller successfulUpdate];
        }else{
            [controller errorInConnectionforPicture];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnectionforPicture];
    }];
}

@end
