//
//  getUserInfo.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLUserInfo.h"

@implementation PPLUserInfo


//Here we get the info for the Active User, we're making four different calls to the server
// Get User Personal info
// Get user dictionary of followers
// Get user dictionary of followed users
// Get user dictionary of read books
+ (void) getInfoforActiveUserforView:(PPLProfileActiveUserViewController *)controlador{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *usersFollowing = [[NSMutableArray alloc] init];
    NSMutableArray *usersFollowed = [[NSMutableArray alloc] init];
    NSMutableArray *readBooks = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/getUsersFollowingMe?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager2 = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager2 setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager2.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager2 GET:[manager2.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFollowing addObject:provisionalUser];
        }
        [controlador successfulConnectionforFollowingUsers:usersFollowing];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
    
    url = [NSString stringWithFormat:@"%@User/getUsersFollowed?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager3 = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager3 setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager3.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager3 GET:[manager3.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFollowed addObject:provisionalUser];
        }
        [controlador successfulConnectionforFollowdUsers:usersFollowed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
    
    url = [NSString stringWithFormat:@"%@User/getReviewedBooks?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager4 = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager4 setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager4.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager4 GET:[manager4.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalBooks = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalBooks.count; i++) {
            NSString *bookCalled = provisionalBooks[i];
            NSDictionary *diccBook = [diccProvisional objectForKey:bookCalled];
            PPLBook *provisionalBook = [[PPLBook alloc] initWithDictionary:[diccBook objectForKey:@"book"]];
            [readBooks addObject:provisionalBook];
        }
        [controlador successfulConnectionforBooks:readBooks];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
    
    url = [NSString stringWithFormat:@"%@User/login?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        if ([[diccionario objectForKey:@"valid"] isEqualToString:@"Yes"]){
            PPLUser *usuarioProvisional = [[PPLUser alloc] initWithDictionary:diccionario];
            [controlador successfulConnectionforUser:usuarioProvisional];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
}

+ (void) getNumbersforOtherUser:(PPLUser *)otherUser andView:(PPLOtherUserViewController *)controlador{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *usersFollowing = [[NSMutableArray alloc] init];
    NSMutableArray *usersFollowed = [[NSMutableArray alloc] init];
    NSMutableArray *readBooks = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/getUsersFollowingMe?pattern=%@&pwd=%@&alternate_user_id=%i", pasionWSBaseUrl, user, psswd, otherUser.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFollowing addObject:provisionalUser];
        }
        [controlador successfulConnectionforFollowingUsers:usersFollowing];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
    
    url = [NSString stringWithFormat:@"%@User/getUsersFollowed?pattern=%@&pwd=%@&alternate_user_id=%i", pasionWSBaseUrl, user, psswd, otherUser.idUser];
    requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager2 = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager2 setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager2.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager2 GET:[manager2.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFollowed addObject:provisionalUser];
        }
        [controlador successfulConnectionforFollowdUsers:usersFollowed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];
    
    url = [NSString stringWithFormat:@"%@User/getReviewedBooks?pattern=%@&pwd=%@&alternate_user_id=%i", pasionWSBaseUrl, user, psswd, otherUser.idUser];
    requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager3 = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager3 setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager3.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager3 GET:[manager3.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalBooks = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalBooks.count; i++) {
            NSString *bookCalled = provisionalBooks[i];
            NSDictionary *diccBook = [diccProvisional objectForKey:bookCalled];
            PPLBook *provisionalBook = [[PPLBook alloc] initWithDictionary:[diccBook objectForKey:@"book"]];
            [readBooks addObject:provisionalBook];
        }
        [controlador successfulConnectionforBooks:readBooks];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorinConnection];
    }];

}

+ (void) searchUserwithString: (NSString *) searchString andView: (PPLSearchUserViewController *) controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *usersFound = [[NSMutableArray alloc] init];

    NSString *url = [NSString stringWithFormat:@"%@User/search?pattern=%@&pwd=%@&search_string=%@&", pasionWSBaseUrl, user, psswd, searchString];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFound addObject:provisionalUser];
        }
        [controller successfulSearchofUsers:usersFound];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

+ (void) searchTeacherwithString:(NSString *)searchString andView:(PPLAskEndorsementViewController *)controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *usersFound = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/search?pattern=%@&pwd=%@&type=F&search_string=%@", pasionWSBaseUrl, user, psswd, searchString];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *provisionalUsers = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < provisionalUsers.count; i++) {
            NSString *userCalled = provisionalUsers[i];
            PPLUser *provisionalUser = [[PPLUser alloc] initWithDictionary:[diccProvisional objectForKey:userCalled]];
            [usersFound addObject:provisionalUser];
        }
        [controller successfulConnectionwithList:usersFound];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

@end
