//
//  ConnectUsersRequest.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLUser.h"
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"
#import "PPLBook.h"
#import "PPLOtherUserViewController.h"
@class PPLOtherUserViewController;

@interface PPLConnectUsersRequest : NSObject

+ (void) checkIfActiveUserisFollowingUser: (PPLUser *) followedUser forView: (PPLOtherUserViewController *) controller;
+ (void) followUser: (PPLUser *) user forView: (PPLOtherUserViewController *) controller;
+ (void) unfollowUser: (PPLUser *) user forView: (PPLOtherUserViewController *) controller;

@end
