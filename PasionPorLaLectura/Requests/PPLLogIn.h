//
//  LogIn.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"
#import "PPLUser.h"
#import "PPLLogInViewController.h"
#import "PPLAppDelegate.h"
@class PPLLogInViewController;
@class PPLAppDelegate;

@interface PPLLogIn : NSObject

+ (void) performLogInforUser: (NSString *) user andPassword: (NSString *) password forView: (PPLLogInViewController *) controlador;
+ (void) checkActiveUser: (PPLAppDelegate *) controlador;

@end