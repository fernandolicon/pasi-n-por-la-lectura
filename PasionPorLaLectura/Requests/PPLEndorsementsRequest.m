//
//  EndorsmentsRequest.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLEndorsementsRequest.h"

@implementation PPLEndorsementsRequest

+ (void) getEndorsmentswithStatus:(NSString *)status forViewController:(PPLPresentEndorsementsViewController *)controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *listEndorsers = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/getEndorsed?pattern=%@&pwd=%@&state=%@", pasionWSBaseUrl, user, psswd, status];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *namesEndorsments = [diccProvisional objectForKey:@"objs"];
        for (int i = 0; i < namesEndorsments.count; i++) {
            NSString *numberofEndorse = namesEndorsments[i];
            PPLEndorsement *provisionalEndorse = [[PPLEndorsement alloc] initWithDictionary:[diccProvisional objectForKey:numberofEndorse]];
            [listEndorsers addObject:provisionalEndorse];
        }
        [controller successfulConnectionwithArray:listEndorsers];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];

}

+ (void) endorseBookforEndorsement: (PPLEndorsement *) endorse forViewController: (PPLSeeEndorsementViewController *) controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@User/endorseBook?pattern=%@&pwd=%@&id_book=%i&id_student=%i", pasionWSBaseUrl, user, psswd, endorse.book.idBook, endorse.user.idUser];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        
        if ([[diccProvisional objectForKey:@"valid"] isEqualToString:@"Yes"]) {
            [controller successfulEndorsement];
        }else{
            [controller errorInConnection];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

+ (void) askEndorsementToTeacher:(int)idTeacher forBook:(int)idBook andView:(PPLAskEndorsementViewController *)controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    
    NSString *url = [NSString stringWithFormat:@"%@User/requestEndorsement?pattern=%@&pwd=%@&id_book=%i&id_faculty=%i", pasionWSBaseUrl, user, psswd, idBook, idTeacher];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];

        if ([[diccProvisional objectForKey:@"status"] isEqualToString:@"OK"]) {
            [controller successfulAskEndorsement];
        }else{
            [controller repeatedEndorsement];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
}

@end
