//
//  LogIn.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLLogIn.h"

@implementation PPLLogIn

//This method is used to perform the Log In of the user

+ (void) performLogInforUser: (NSString *) user andPassword: (NSString *) password forView: (PPLLogInViewController *) controlador{
    NSString *url = [NSString stringWithFormat:@"%@User/login?pattern=%@&pwd=%@", pasionWSBaseUrl, user, password];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    
    //We make the GET request to the Web service without paramaters, we don't need them
    //In case that the User is valid we return the data to the controller in order to complete the Log In
    //If the password or user is wrong we call to the error with data, asking the user to try again
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        if ([[diccionario objectForKey:@"valid"] isEqualToString:@"Yes"]){
            PPLUser *provisional = [[PPLUser alloc] initWithDictionary:diccionario];
            [controlador sucessfullConnectionforUser:provisional withUserName:user withPassword:password];
        }else{
            [controlador errorwithData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorwithConnection];
    }];
}


//This method runs every time the user starts the app in order to check if the user stored is valid
+ (void) checkActiveUser:(PPLAppDelegate *)controlador{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSString *url = [NSString stringWithFormat:@"%@User/login?pattern=%@&pwd=%@", pasionWSBaseUrl, user, psswd];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccionario = [responseObject objectForKey:@"response"];
        if ([[diccionario objectForKey:@"valid"] isEqualToString:@"No"]){
            [controlador errorinData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controlador errorInConnection];
    }];
}

@end
