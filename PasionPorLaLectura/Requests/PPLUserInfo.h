//
//  getUserInfo.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLUser.h"
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"
#import "PPLProfileActiveUserViewController.h"
#import "PPLSearchUserViewController.h"
#import "PPLBook.h"
#import "PPLAskEndorsementViewController.h"
#import "PPLOtherUserViewController.h"
@class PPLProfileActiveUserViewController;
@class PPLSearchUserViewController;
@class PPLAskEndorsementViewController;
@class PPLOtherUserViewController;

@interface PPLUserInfo : NSObject

@property int counter;

+ (void) getInfoforActiveUserforView: (PPLProfileActiveUserViewController *) controlador;
+ (void) getNumbersforOtherUser: (PPLUser *) otherUser andView: (PPLOtherUserViewController *) controlador;
+ (void) searchUserwithString: (NSString *) searchString andView: (PPLSearchUserViewController *) controller;
+ (void) searchTeacherwithString: (NSString *) searchString andView: (PPLAskEndorsementViewController *) controller;

@end
