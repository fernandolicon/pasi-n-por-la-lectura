//
//  SearchBookViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLCeldaLibro.h"
#import "PPLBook.h"
#import "PPLBooksReqests.h"
#import "PPLBookViewController.h"
#import "PPLAppDelegate.h"

@interface PPLSearchBookViewController : UIViewController <UISearchBarDelegate>{
    UIApplication *networkIdentifier;
    
    PPLBook *slctdBook;
}

@property (nonatomic, strong) NSMutableArray *listBooks;

- (void) successfulSearchofBooks: (NSMutableArray *) foundBooks;
- (void) errorInConnection;

@end
