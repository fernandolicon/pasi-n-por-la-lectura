//
//  MyBooksViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLMyBooksViewController.h"
#import "SVProgressHUD.h"

@interface PPLMyBooksViewController ()

@end

@implementation PPLMyBooksViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    networkIdentifier = [UIApplication sharedApplication];
    networkIdentifier.networkActivityIndicatorVisible = YES;
    
    refreshInfo = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:refreshInfo];
    [refreshInfo addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    //BOOL in order to know if a refresh was performed by the user
    reload = NO;
    emptyBooks = NO;
    
    //Don't present data until all conections were performed
    whiteView = [[UIView alloc] initWithFrame: CGRectMake ( 0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [whiteView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:whiteView];
    [self.view bringSubviewToFront:whiteView];
    
    [SVProgressHUD show];
    
    
    if (!myBooks) {
        myBooks = [[NSMutableArray alloc] init];
    }
    
    self.tableView.separatorColor = [UIColor clearColor];
    
    [self performConnection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NetworkConnections

- (void) performConnection{
    [PPLBooksReqests getBooksReadedbyActiveUserforView:self];
}

- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead{
    whiteView.hidden = YES;
    [SVProgressHUD dismiss];
    
    networkIdentifier.networkActivityIndicatorVisible = NO;
    myBooks = booksRead;
    
    if (myBooks.count != 0) {
        emptyBooks = NO;
    }
    
    if (reload) {
        [refreshInfo endRefreshing];
        reload = NO;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        [self.tableView reloadData];
    }
}

- (void) errorinConnection{
    whiteView.hidden = YES;
    [SVProgressHUD dismiss];
    
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Reload Data

- (void)refreshTable {
    reload = YES;
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [PPLBooksReqests getBooksReadedbyActiveUserforView:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (myBooks.count == 0){
        emptyBooks = YES;
        return 1;
    }else{
        return myBooks.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (emptyBooks) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"booksCell"];
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"booksCell"];
        }
        
        cell.textLabel.text = @"No se encontro ningún libro";
        return cell;
    }
    
    PPLCeldaLibro *cell = [tableView dequeueReusableCellWithIdentifier:@"bookCell"];
    PPLBook *provisionalBook = [myBooks objectAtIndex:indexPath.row];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLCeldaLibro" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.nombre.text = provisionalBook.name;
    cell.autor.text = provisionalBook.author;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    slctdBook = [myBooks objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ViewReadBook" sender:self];
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    PPLBookViewController *nextVC = [segue destinationViewController];
    nextVC.selectedBook = slctdBook;
}

#pragma mark - Delegate Methods

- (void)getNewBook{
    [self performConnection];
}


@end
