//
//  OneHundredViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/2/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLOneHundredViewController.h"

@interface PPLOneHundredViewController ()

@end

@implementation PPLOneHundredViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    listBooks = [[NSMutableArray alloc] init];
    [listBooks addObject:@"1. Al filo del agua - Agustín Yáñez"];
    [listBooks addObject:@"2. Amor perdido - Carlos Monsiváis"];
    [listBooks addObject:@"3. Ampliación del campo de batalla - Michel Houellebecq"];
    [listBooks addObject:@"4. Antología de la literatura fantástica - J. L. Borges, Adolfo Bioy Casares y Silvina Ocampo"];
    [listBooks addObject:@"5. Aura - Carlos Fuentes"];
    [listBooks addObject:@"6. Balún Canán - Rosario Castellanos"];
    [listBooks addObject:@"7. Biografía del poder - Enrique Krauze"];
    [listBooks addObject:@"8. Cándido – Voltaire"];
    [listBooks addObject:@"9. Cantar del mío Cid – Anónimo"];
    [listBooks addObject:@"10. Cien años de soledad - Gabriel García Márquez"];
    [listBooks addObject:@"11. Confabulario - Juan José Arreola"];
    [listBooks addObject:@"12. Crimen y castigo - F. M. Dostoievski"];
    [listBooks addObject:@"13. Cuentos - Horacio Quiroga"];
    [listBooks addObject:@"14. Cumbres borrascosas - Emily Brönte"];
    [listBooks addObject:@"15. Desgracia - J. M. Coetzee"];
    [listBooks addObject:@"16. Don Quijote de la Mancha - Miguel de Cervantes"];
    [listBooks addObject:@"17. Drácula - Bram Stoker"];
    [listBooks addObject:@"18. Dublineses - James Joyce"];
    [listBooks addObject:@"19. Edipo rey – Sófocles"];
    [listBooks addObject:@"20. El conde de Montecristo - Alejandro Dumas, Padre"];
    [listBooks addObject:@"21. El corazón de las tinieblas - Joseph Conrad"];
    [listBooks addObject:@"22. El extranjero - Albert Camus"];
    [listBooks addObject:@"23. El extraño caso del Dr. Jekyll y Mr. Hyde - Robert Louis Stevenson"];
    [listBooks addObject:@"24. El guardián entre el centeno - J. D. Salinger"];
    [listBooks addObject:@"25. El juguete rabioso - Roberto Arlt"];
    [listBooks addObject:@"26. El laberinto de la soledad - Octavio Paz"];
    [listBooks addObject:@"27. El maestro y Margarita - Mijail Bulgákov"];
    [listBooks addObject:@"28. El muro - Jean Paul Sarte"];
    [listBooks addObject:@"29. El nombre de la rosa - Umberto Eco"];
    [listBooks addObject:@"30. El perfume - Patrick Süskind"];
    [listBooks addObject:@"31. El pozo - Juan Carlos Onetti"];
    [listBooks addObject:@"32. El Principito - Antoine de Saint-Exupéry"];
    [listBooks addObject:@"33. El retrato de Dorian Gray - Oscar Wilde"];
    [listBooks addObject:@"34. El señor de las moscas - William Golding"];
    [listBooks addObject:@"35. El Señor de los Anillos - J.R.R. Tolkien"];
    [listBooks addObject:@"36. El túnel - Ernesto Sabato"];
    [listBooks addObject:@"37. El último encuentro - Sándor Márai"];
    [listBooks addObject:@"38. El Zarco - Ignacio M. Altamirano"];
    [listBooks addObject:@"39. Ensayo sobre la ceguera - José Saramago"];
    [listBooks addObject:@"40. Este lado del paraíso - F. S. Fitzgerald"];
    [listBooks addObject:@"41. Fahrenheit 451 - Ray Bradbury"];
    [listBooks addObject:@"42. Ficciones - Jorge Luis Borges"];
    [listBooks addObject:@"43. Frankenstein - Mary W. Shelley"];
    [listBooks addObject:@"44. Hamlet - William Shakespeare"];
    [listBooks addObject:@"45. La casa de Bernarda Alba - Federico García Lorca"];
    [listBooks addObject:@"46. La casa de las bellas durmientes - Yasunari Kawabata"];
    [listBooks addObject:@"47. La Celestina - Fernando de Rojas"];
    [listBooks addObject:@"48. La divina comedia - Dante Alighieri"];
    [listBooks addObject:@"49. La fiesta del chivo - Mario Vargas Llosa"];
    [listBooks addObject:@"50. La insoportable levedad del ser - Milan Kundera"];
    [listBooks addObject:@"51. La metamorfosis - Franz Kafka"];
    [listBooks addObject:@"52. La muerte de Iván Ilich - León Tolstoi"];
    [listBooks addObject:@"53. La muerte en Venecia - Thomas Mann"];
    [listBooks addObject:@"54. La noche de Tlatelolco - Elena Poniatowska"];
    [listBooks addObject:@"55. La piel de zapa – Balzac"];
    [listBooks addObject:@"56. La señora Dalloway - Virginia Woolf"];
    [listBooks addObject:@"57. La sombra del caudillo - Martín Luis Guzmán"];
    [listBooks addObject:@"58. La tregua - Mario Benedetti"];
    [listBooks addObject:@"59. La vida es sueño - Pedro Calderón de la Barca"];
    [listBooks addObject:@"60. La visión de los vencidos - Miguel León Portilla"];
    [listBooks addObject:@"61. Las batallas en el desierto - José Emilio Pacheco"];
    [listBooks addObject:@"62. Las flores del mal - Charles Baudelaire"];
    [listBooks addObject:@"63. Las relaciones peligrosas - Choderlos de Laclos"];
    [listBooks addObject:@"64. Las tribulaciones del estudiante Törless - Robert Musil"];
    [listBooks addObject:@"65. Lazarillo de Torme – Anónimo"];
    [listBooks addObject:@"66. Libro del desasosiego - Fernando Pessoa"];
    [listBooks addObject:@"67. Lírica personal - Sor Juana Inés de la Cruz"];
    [listBooks addObject:@"68. Lolita - Vladimir Nabokov"];
    [listBooks addObject:@"69. Los de abajo - Mariano Azuela"];
    [listBooks addObject:@"70. Los detectives salvajes - Roberto Bolaño"];
    [listBooks addObject:@"71. Los miserables - Víctor Hugo"];
    [listBooks addObject:@"72. Los recuerdos del porvenir - Elena Garro"];
    [listBooks addObject:@"73. Los relámpagos de agosto - Jorge Ibargüengoitia"];
    [listBooks addObject:@"74. Madame Bovary - Gustave Flaubert"];
    [listBooks addObject:@"75. Memorias - Fray Servando Teresa de Mier"];
    [listBooks addObject:@"76. Memorias de Adriano - Marguerite Yourcenar"];
    [listBooks addObject:@"77. México - Alfonso Reyes"];
    [listBooks addObject:@"78. Moby Dick - Herman Melville"];
    [listBooks addObject:@"79. Muerte sin fin - José Gorostiza"];
    [listBooks addObject:@"80. Nada - Carmen Laforet"];
    [listBooks addObject:@"81. Narraciones extraordinarias - Edgar Allan Poe"];
    [listBooks addObject:@"82. Noticias del imperio - Fernando del Paso"];
    [listBooks addObject:@"83. Nueva historia mínima de México - El Colegio de México"];
    [listBooks addObject:@"84. Odisea – Homero"];
    [listBooks addObject:@"85. Orgullo y prejuicio - Jane Austen"];
    [listBooks addObject:@"86. Pedro Páramo - Juan Rulfo"];
    [listBooks addObject:@"87. Poesía en movimiento - Octavio Paz"];
    [listBooks addObject:@"88. Rayuela - Julio Cortázar"];
    [listBooks addObject:@"89. Rojo y negro – Stendhal"];
    [listBooks addObject:@"90. Seis personajes en busca de autor - Luigi Pirandello"];
    [listBooks addObject:@"91. Siddharta - Herman Hesse"];
    [listBooks addObject:@"92. Sostiene Pereira - Antonio Tabucchi"];
    [listBooks addObject:@"93. Suave patria - Ramón López Velarde"];
    [listBooks addObject:@"94. Sylvie - Gérard de Nerval"];
    [listBooks addObject:@"95. Tokyo blues - Haruki Murakami"];
    [listBooks addObject:@"96. Ulises criollo - José Vasconcelos"];
    [listBooks addObject:@"97. Un mundo feliz - Aldous Huxley"];
    [listBooks addObject:@"98. Veinte poemas de amor y una canción desesperada - Pablo Neruda"];
    [listBooks addObject:@"99. Werther - J. W. Goethe"];
    [listBooks addObject:@"100. 1984 - George Orwell"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listBooks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"OtherBooksCell"];
    
    if (celda == nil){
        celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"OtherBooksCell"];
    }
    
    celda.textLabel.text = [listBooks objectAtIndex:indexPath.row];
    
    return celda;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Libro Seleccionado" message:[NSString stringWithFormat:@"%@", listBooks[indexPath.row]] delegate:nil cancelButtonTitle:@"Continuar" otherButtonTitles:nil];
    [alert show];
}

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
