//
//  SeeEndorsementViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPCRateView.h"
#import "PPLEndorsement.h"
#import "PPLReviewRequest.h"
#import "PPLReview.h"

@protocol SeeEndorsementDelegate

- (void)removeEndorsefromList:(PPLEndorsement *) removeEndorse;

@end

#import "PPLEndorsementsRequest.h"


@interface PPLSeeEndorsementViewController : UIViewController <RateViewDelegate>{
    
    __weak IBOutlet UILabel *txtLibro;
    __weak IBOutlet UILabel *txtAutor;
    __weak IBOutlet UILabel *txtAlumno;
    __weak IBOutlet UILabel *txtMatricula;
    __weak IBOutlet UITextView *txtReview;
    __weak IBOutlet UIBarButtonItem *bttnEndosar;
    __weak IBOutlet UIImageView *imageUser;
    
    UIApplication *networkIdentifier;
}


@property (nonatomic, strong) PPLEndorsement *endorsement;
- (IBAction)endosar:(id)sender;

@property (nonatomic) id<SeeEndorsementDelegate> delegate;

@property (weak, nonatomic) IBOutlet TPCRateView *rateView;

- (void) successfulConnectionwithReview: (PPLReview *) review;
- (void) successfulEndorsement;
- (void) errorInConnection;

@end
