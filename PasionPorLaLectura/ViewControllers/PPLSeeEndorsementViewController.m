//
//  SeeEndorsementViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLSeeEndorsementViewController.h"

@interface PPLSeeEndorsementViewController ()

@end

@implementation PPLSeeEndorsementViewController

@synthesize rateView;
@synthesize endorsement;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*
    NSDictionary *reviews = [self getReview];
    NSDictionary *review = [reviews objectForKey:@"bookreview-0"];*/
    
    if([endorsement.status isEqualToString:@"E"]){
        [bttnEndosar setEnabled: NO];
        [bttnEndosar setTitle:@""];
    }
    
    self.title = endorsement.user.userKey;
    
    NSString *nombre_alumno = [NSString stringWithFormat:@"%@ %@", endorsement.user.firstName, endorsement.user.lastName];
    txtAlumno.text = nombre_alumno;
    txtMatricula.text = endorsement.user.userKey;
    txtLibro.text = endorsement.book.name;
    txtAutor.text = endorsement.book.author;
    
    rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
    rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
    rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    rateView.rating = 0;
    rateView.maxRating = 5;
    rateView.delegate = self;
    
    networkIdentifier = [UIApplication sharedApplication];
    networkIdentifier.networkActivityIndicatorVisible = YES;
    
    [self performConnection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) rateView:(TPCRateView *)rateView ratingDidChange:(float)rating{
    
}

#pragma mark - NetworkConnections

- (void) performConnection{
    [PPLReviewRequest getReviewforEndorsement:endorsement andView:self];
}

- (void) successfulConnectionwithReview:(PPLReview *)review{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    rateView.rating = review.rate;
    txtReview.text = review.reviewText;
    [txtReview setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=70", [PPLUser getUserName], [PPLUser getPassword], review.user.idUser];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    imageUser.image = [UIImage imageWithData:imageData];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void) successfulEndorsement{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Correcto" message:@"El endosado se ha realizado con éxito" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    networkIdentifier.networkActivityIndicatorVisible = NO;
    [self.delegate removeEndorsefromList:endorsement];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)endosar:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Endorsar" message:@"¿Esta seguro de querer endosar el libro?" delegate:self cancelButtonTitle:@"Si" otherButtonTitles:@"No", nil];
    [alert show];
    [bttnEndosar setEnabled: NO];
    [bttnEndosar setTitle:@""];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        networkIdentifier.networkActivityIndicatorVisible = YES;
        [PPLEndorsementsRequest endorseBookforEndorsement:endorsement forViewController:self];
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}

@end
