//
//  ReviewViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLReviewViewController.h"

@interface PPLReviewViewController ()

@end

@implementation PPLReviewViewController

@synthesize rateView;
@synthesize lastReview;
@synthesize idReviewBook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    networkIdentifier = [UIApplication sharedApplication];
    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    self.rateView.rating = 0;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;
    
    self.title = @"Escribir reseña";
    
    rateView.rating = lastReview.rate;
    txtResena.text = lastReview.reviewText;
    lblCharCount.text = [NSString stringWithFormat:@"%lu/120", (unsigned long)lastReview.reviewText.length];
    [txtResena setFont:[UIFont fontWithName:@"Helvetica" size:18]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textViewShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    lblCharCount.text = [NSString stringWithFormat:@"%lu/120", (unsigned long)txtResena.text.length];
    
    if (textView.text.length > 120) {
        lblCharCount.textColor = [UIColor redColor];
    }else{
        lblCharCount.textColor = [UIColor blackColor];
    }
}

#pragma mark - RateView

- (void)rateView:(TPCRateView *)rateView ratingDidChange:(float)rating {
    //self.statusLabel.text = [NSString stringWithFormat:@"Rating: %f", rating];
}

#pragma mark - TouchGestures

- (void) dismissKeyboard{
    [txtResena resignFirstResponder];
}

#pragma mark - NetworkConnections

- (void) successfulConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡Hecho!" message:@"La reseña ha sido actualizada" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [self.delegate getNewReview];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - ButtonActions

- (void) enviarResena:(id)sender{
    if (txtResena.text.length > 120) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reseña muy larga" message:@"La reseña debe ser menor de 120 carácteres" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
        [alert show];
    }else{
        networkIdentifier.networkActivityIndicatorVisible = YES;
        NSData *reviewEncode = [txtResena.text dataUsingEncoding:NSUTF8StringEncoding];
        NSString *reviewEncoded = [reviewEncode base64EncodedStringWithOptions:0];
        int rateInt = (int) rateView.rating;
        [PPLReviewRequest setReview:reviewEncoded withRate:rateInt forBook:idReviewBook andView:self];
    }
}

@end
