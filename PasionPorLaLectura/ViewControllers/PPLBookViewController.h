//
//  BookViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewBookDelegate

- (void)getNewBook;

@end

#import "PPLBook.h"
#import "PPLUser.h"
#import "PPLReview.h"
#import "PPLReviewViewController.h"
#import "PPLAskEndorsementViewController.h"
#import "PPLMyReviewTableViewCell.h"
#import "PPLLastReviewTableViewCell.h"
#import "PPLAllReviewsTableViewController.h"

@interface PPLBookViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NewMyReviewDelegate>{
    __weak IBOutlet UILabel *txtNombreLibro;
    __weak IBOutlet UILabel *txtAutor;
    __weak IBOutlet UILabel *txtEdicion;
    __weak IBOutlet UILabel *txtEditor;
    __weak IBOutlet UILabel *txtISBN;
    __weak IBOutlet UILabel *txtYear;
    __weak IBOutlet UILabel *txt_category_name;
    __weak IBOutlet UITableView *tblView;
    
    __weak IBOutlet UIScrollView *scrllView;
    
    UIApplication *networkIdentifier;
    
    NSMutableDictionary *lastReviewsImages; //used to storage images
    
    BOOL isTeacher; //Used to hide enrosement cells
    BOOL hasOwnReview;
    int counterofReviews;
    int newReviews; //This will be used to know how many new reviews we have
    float heightofTableView;
    BOOL newBook;
    
    NSMutableArray *actions;
    NSMutableArray *reviews;
}

@property (nonatomic, strong) PPLBook *selectedBook;
@property (nonatomic, strong) PPLReview *reviewOfBook;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;

@property (nonatomic) id<NewBookDelegate> delegate;

- (void)agregarNuevaR;
- (void)requestE;

- (void) successfulConnectionwithReview: (PPLReview *) review;
- (void) successfulConnectionwithLastReviews: (NSMutableArray *) lastReviews;
- (void) errorInConnection;

@end
