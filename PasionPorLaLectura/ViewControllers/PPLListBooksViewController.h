//
//  ListBooksViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLCeldaLibro.h"
#import "PPLBook.h"
#import "PPLBookViewController.h"

@interface PPLListBooksViewController : UITableViewController{
    PPLBook *slctdBook;
}

@property (nonatomic, strong) NSMutableArray *listBooks;

@end
