//
//  FollowersViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 1/31/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CeldaUsuario/PPLCeldaUsuario.h"
#import "PPLUser.h"
#import "PPLOtherUserViewController.h"

@interface PPLFollowersViewController : UITableViewController{
    PPLUser *selectedUser;
    UIApplication *networkIndicator;
    NSMutableDictionary *imageUsers;
}

@property (nonatomic, strong) NSMutableArray *listUsers;
@property (nonatomic, strong) NSString *titleView;

@end
