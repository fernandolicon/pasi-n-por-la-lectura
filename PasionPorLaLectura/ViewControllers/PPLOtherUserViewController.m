//
//  OtherUserViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLOtherUserViewController.h"
#import "PPLAppDelegate.h"

@interface PPLOtherUserViewController ()

@end

@implementation PPLOtherUserViewController

@synthesize otherUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    networkIndicator = [UIApplication sharedApplication];
	// Do any additional setup after loading the view.
    
    self.title = [NSString stringWithFormat:@"%@", otherUser.firstName];
    
    //Refresh view configuration
    refreshUser = [[UIRefreshControl alloc] init];
    [scrllView addSubview:refreshUser];
    reload = NO;
    [refreshUser addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    
    connectionsCounter = 0;
    if(otherUser.idUser == [PPLUser getIdUser]){
        btnFollow.title = @"";
        btnFollow.enabled = NO;
    }
    
    [self performConnection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIActions

- (IBAction)actionForUser:(id)sender{
    if([btnFollow.title isEqualToString:@"Seguir"]){
        [PPLConnectUsersRequest followUser:otherUser forView:self];
    }else{
        [PPLConnectUsersRequest unfollowUser:otherUser forView:self];
    }
}

- (IBAction)viewFollowers:(id)sender {
    nextViewTitle = @"Seguidores";
    sentUsers = listUsrFollowing;
    [self performSegueWithIdentifier:@"UsersfoOU" sender:sender];
}

- (IBAction)viewFollowing:(id)sender {
    nextViewTitle = @"Siguiendo";
    sentUsers = listUsrFollowers;
    [self performSegueWithIdentifier:@"UsersfoOU" sender:sender];
}

- (IBAction)viewReadBooks:(id)sender {
    [self performSegueWithIdentifier:@"PresentReadBooksOU" sender:sender];
}


#pragma mark - NetworkConnection

//In order to perform the update of the view when all the info from the requests has arrived
//we have a counter of connections, when this counter is equal to 2 it means we are in the last
//request, so now we can update the view with the received data.

- (void) performConnection{
    networkIndicator.networkActivityIndicatorVisible = YES;
    [PPLUserInfo getNumbersforOtherUser:otherUser andView:self];
    [PPLConnectUsersRequest checkIfActiveUserisFollowingUser:otherUser forView:self];
}

- (void) successfulConnectionforFollowingUsers: (NSMutableArray *) usersFollowing{
    listUsrFollowing = usersFollowing;
    if (connectionsCounter == 2){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) successfulConnectionforFollowdUsers: (NSMutableArray *) usersFollowed{
    listUsrFollowers = usersFollowed;
    if (connectionsCounter == 2){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead{
    listReadBooks = booksRead;
    if (connectionsCounter == 2){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) userIsFollowing{
    btnFollow.title = @"Dejar de Seguir";
}

- (void) correctFollowUser{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Siguiendo Usuario" message:[NSString stringWithFormat:@"¡Se ha empezado a seguir a %@!", otherUser.firstName]delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    btnFollow.title = @"Dejar de Seguir";
    int seguidores = [txtSeguidores.text intValue];
    PPLAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.newFollow = YES;
    seguidores += 1;
    txtSeguidores.text = [NSString stringWithFormat:@"%i",seguidores];
}

- (void) correctUnfollowUser{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Correcto" message:[NSString stringWithFormat:@"Se ha dejado de seguir a %@", otherUser.firstName]delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    btnFollow.title = @"Seguir";
    int seguidores = [txtSeguidores.text intValue];
    seguidores -= 1;
    txtSeguidores.text = [NSString stringWithFormat:@"%i",seguidores];
    
    for (int i = 0; i < listUsrFollowing.count; i++) {
        PPLUser *provisionalUsr = [listUsrFollowing objectAtIndex:i];
        if (provisionalUsr.idUser == [PPLUser getIdUser]) {
            [listUsrFollowing removeObjectAtIndex:i];
        }
    }
    
}

- (void) allConnectionsPerformed{
    networkIndicator.networkActivityIndicatorVisible = NO;
    
    txtNombre.text = [NSString stringWithFormat:@"%@ %@", otherUser.firstName, otherUser.lastName];
    txtCita.text = otherUser.quote;
    
    txtPrograma.text = otherUser.program;
    [txtNombre setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [txtCita setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    
    NSString *conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listUsrFollowing.count ];
    txtSeguidores.text = conteo;
    
    conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listUsrFollowers.count ];
    txtSiguiendo.text = conteo;
    
    conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listReadBooks.count ];
    txtLeidos.text = conteo;
    
    NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=115", [PPLUser getUserName], [PPLUser getPassword], otherUser.idUser];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    imgPerfil.image = [UIImage imageWithData:imageData];
    
    if (reload) {
        [refreshUser endRefreshing];
        reload = NO;
    }
    
    
}

- (void) errorinConnection{
    if (connectionsCounter == 0){
        networkIndicator.networkActivityIndicatorVisible = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión. Intente más tarde." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        connectionsCounter ++;
    }
}

#pragma mark - Reload Data

- (void)refreshView {
    reload = YES;
    networkIndicator.networkActivityIndicatorVisible = YES;
    connectionsCounter = 0;
    [self performConnection];
}

#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"UsersfoOU"]) {
        PPLFollowersViewController *followView = [segue destinationViewController];
        followView.listUsers = sentUsers;
        followView.titleView = nextViewTitle;
    }
    if ([segue.identifier isEqualToString:@"PresentReadBooksOU"]) {
        PPLListBooksViewController *followView = [segue destinationViewController];
        followView.listBooks = listReadBooks;
    }
    
}

@end
