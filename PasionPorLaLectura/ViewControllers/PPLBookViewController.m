//
//  BookViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLBookViewController.h"

@interface PPLBookViewController ()

@end

@implementation PPLBookViewController

@synthesize selectedBook;
@synthesize reviewOfBook;
@synthesize viewHeightConstraint;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UITabBarController *tabBarController = [self tabBarController];
    if ([tabBarController.viewControllers objectAtIndex:3]) {
        self.delegate = [tabBarController.viewControllers objectAtIndex:3];
    }
    
    txtNombreLibro.text = selectedBook.name;
    txtAutor.text = selectedBook.author;
    txtEdicion.text = selectedBook.edition;
    txtEditor.text = selectedBook.editor;
    txtISBN.text = selectedBook.ISBN;
    txtYear.text = [NSString stringWithFormat:@"%li", (long)selectedBook.year];
    txt_category_name.text = selectedBook.subject;
    
    self.title = selectedBook.name;
    
    tblView.scrollEnabled = NO;
    hasOwnReview = NO;
    
    heightofTableView = 0; //Used to update size of table view
    counterofReviews = 0;
    lastReviewsImages = [[NSMutableDictionary alloc] init];
    
    newBook = NO;
    
    actions = [[NSMutableArray alloc] initWithObjects:@"Mi calificación: ", nil];
    
    reviews = [[NSMutableArray alloc] init];
    
    networkIdentifier = [UIApplication sharedApplication];
    
    [self performConnection];
    
    if([[PPLUser getUserType] isEqualToString:@"F"]){
        isTeacher = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    if (newBook) {
        //[self.delegate getNewBook];
    }
}

#pragma mark - Buttons action

- (void) agregarNuevaR{
    [self performSegueWithIdentifier:@"WriteReview" sender:self];
}

- (void) requestE{
    [self performSegueWithIdentifier:@"AskEndorsement" sender:self];
}

- (void) presentAllReviews{
    [self performSegueWithIdentifier:@"SeeAllReviews" sender:self];
}

#pragma mark- Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"WriteReview"]) {
        PPLReviewViewController *nextVC = [segue destinationViewController];
        nextVC.idReviewBook = selectedBook.idBook;
        nextVC.lastReview = reviewOfBook;
        nextVC.delegate = self;
    }
    if ([segue.identifier isEqualToString:@"AskEndorsement"]) {
        PPLAskEndorsementViewController *nextVC = [segue destinationViewController];
        nextVC.idBook = selectedBook.idBook;
    }
    if ([segue.identifier isEqualToString:@"SeeAllReviews"]){
        PPLAllReviewsTableViewController *nextVC = [segue destinationViewController];
        nextVC.idBook = selectedBook.idBook;
        nextVC.nameBook = selectedBook.name;
        nextVC.author = selectedBook.author;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (isTeacher) {
        return 2;
    }
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 1 && !isTeacher) {
        return 1;
    }
    if (((section == 1) && (isTeacher)) || (section == 2)) {
        if (reviews.count == 0){
            return 0;
        }else{
        return reviews.count + 1;
        }
    }
        return 2;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"Mi Reseña";
            break;
        case 1:
            if (isTeacher) {
                return @"Ultima reseña";
            }
            return @"Endoso";
            break;
        case 2:
            return @"Ultima reseña";
            break;
        default:
            return nil;
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookInfoCell"];
    
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0) {
                PPLMyReviewTableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"MyReviewCell"];
                
                if (newCell == nil){
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLMyReviewTableViewCell" owner:self options:nil];
                    newCell = [nib objectAtIndex:0];
                }
                
                newCell.selectionStyle = UITableViewCellSelectionStyleNone;
                newCell.ownReview.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
                newCell.ownReview.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
                newCell.ownReview.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
                newCell.ownReview.rating = 0;
                newCell.ownReview.maxRating = 5;
                newCell.ownReview.delegate = newCell;
                newCell.ownReview.editable = NO;
                
                newCell.ownReview.rating = reviewOfBook.rate;
                
                newCell.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                newCell.ownReview.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                
                return newCell;
            }else{
                if (cell == nil){
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MoreCell"];
                }
                
                if (hasOwnReview) {
                    cell.textLabel.text = @"Editar reseña";
                }else{
                    cell.textLabel.text = @"Escribir reseña";
                }
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            break;
        case 1:
            if (isTeacher) {
                //something else
                if (indexPath.row == 1) {
                    cell.textLabel.text = @"Ver todas las reseñas";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }else{
                
                PPLLastReviewTableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"LastReviewCell"];
                
                if (newCell == nil){
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLLastReviewTableViewCell" owner:self options:nil];
                    newCell = [nib objectAtIndex:0];
                }
                PPLReview *provisionalReview = [reviews objectAtIndex:indexPath.row];
                
                newCell.userName.text = [NSString stringWithFormat:@"%@ %@", provisionalReview.user.firstName, provisionalReview.user.lastName];
                newCell.userReview.text = provisionalReview.reviewText;
                
                [newCell.userReview setFont:[UIFont fontWithName:@"Helvetica" size:14]];
                
                newCell.selectionStyle = UITableViewCellSelectionStyleNone;
                newCell.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
                newCell.rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
                newCell.rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
                newCell.rateView.rating = 0;
                newCell.rateView.maxRating = 5;
                newCell.rateView.delegate = newCell;
                newCell.rateView.editable = NO;
                
                newCell.rateView.rating = provisionalReview.rate;
                
                NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=70", [PPLUser getUserName], [PPLUser getPassword], provisionalReview.user.idUser];
                NSURL *url = [[NSURL alloc] initWithString:urlString];
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                newCell.imgUser.image = [UIImage imageWithData:imageData];
                
                newCell.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                newCell.userReview.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                newCell.rateView.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                
                return newCell;
                }
            }else{
            cell.textLabel.text = @"Pedir endoso";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            break;
        case 2:
            if (indexPath.row == 1) {
                cell.textLabel.text = @"Ver todas las reseñas";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }else{
                
                PPLLastReviewTableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"LastReviewCell"];
                
                if (newCell == nil){
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLLastReviewTableViewCell" owner:self options:nil];
                    newCell = [nib objectAtIndex:0];
                }
                PPLReview *provisionalReview = [reviews objectAtIndex:indexPath.row];
                
                newCell.userName.text = [NSString stringWithFormat:@"%@ %@", provisionalReview.user.firstName, provisionalReview.user.lastName];
                newCell.userReview.text = provisionalReview.reviewText;
                
                [newCell.userReview setFont:[UIFont fontWithName:@"Helvetica" size:14]];
                
                newCell.selectionStyle = UITableViewCellSelectionStyleNone;
                newCell.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
                newCell.rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
                newCell.rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
                newCell.rateView.rating = 0;
                newCell.rateView.maxRating = 5;
                newCell.rateView.delegate = newCell;
                newCell.rateView.editable = NO;
                
                newCell.rateView.rating = provisionalReview.rate;
                
                NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=70", [PPLUser getUserName], [PPLUser getPassword], provisionalReview.user.idUser];
                NSURL *url = [[NSURL alloc] initWithString:urlString];
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                newCell.imgUser.image = [UIImage imageWithData:imageData];
                
                newCell.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                newCell.userReview.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                newCell.rateView.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
                
                return newCell;
            }
                break;
        default:
            break;
    }
    
    cell.backgroundColor = [UIColor colorWithRed:255/255.f green:253/255.f blue:250/255.f alpha:1.0];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ((indexPath.section == 0) && (indexPath.row == 1)) {
        [self agregarNuevaR];
    }
    if ((indexPath.section == 1) && (!isTeacher)) {
        [self requestE];
    }
    
    if ([cell.textLabel.text isEqualToString: @"Ver todas las reseñas"]) {
        [self presentAllReviews];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (((indexPath.section == 1) && (isTeacher)) || (indexPath.section == 2)) {
        if ((reviews.count != 0) && (indexPath.row == 1)){
            return 44;
        }else{
            return 150;
        }
    }
    
    return 44;
}

#pragma mark - NewSizes
//Here we update size of scrollview and table view
/*
- (void) reloadSizes{
    NSLog(@"actual height %f", heightofTableView);
    float height = heightofTableView + (newReviews * 150);
    NSLog(@"%i", newReviews);
    
    tblView.frame = CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y, tblView.frame.size.width, height);
    tblView.bounds = CGRectMake(0, 0, tblView.bounds.size.width, height);
    [tblView setNeedsDisplay];
    NSLog(@"Supposed height %f", height);
    NSLog(@"Real height %f", tblView.frame.size.height);
    float heightofScrollView = tblView.frame.origin.y + height;
    //scrllView.contentSize = CGSizeMake(scrllView.contentSize.width, heightofScrollView);
    scrllView.bounds = CGRectMake(0, 0, scrllView.bounds.size.width, heightofScrollView);
    scrllView.frame = CGRectMake(0, 0, scrllView.frame.size.width, heightofScrollView);
    [scrllView setNeedsDisplay];
    NSLog(@"height of scroll view %f", heightofScrollView);
    NSLog(@"real heigh of scroll view %f", scrllView.frame.size.height);
    [scrllView setNeedsDisplay];
    
    [self.view setFrame:scrllView.frame];
    [self.view setBounds:scrllView.bounds];
    [self.view updateConstraintsIfNeeded];
    [self.view setNeedsDisplay];
    NSLog(@"View height %f", self.view.frame.size.height);
}*/

#pragma mark - NetworkConnections

- (void) performConnection{
    heightofTableView = tblView.contentSize.height;
    [self getLastReview];
    [self getLastReviews];
}

- (void) getLastReview{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [PPLReviewRequest getLastReviewforIdBook:selectedBook.idBook andView:self];
}

- (void) getLastReviews{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [PPLReviewRequest getLastReviewsforBook:selectedBook.idBook fromReviewNumber:counterofReviews numberofReviews:1 forView:self];
}

- (void) successfulConnectionwithReview:(PPLReview *)review{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    hasOwnReview = YES;
    reviewOfBook = review;
    [tblView reloadData];
}

- (void) successfulConnectionwithLastReviews: (NSMutableArray *) lastReviews{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    reviews = lastReviews;
    [tblView reloadData];
}

- (void) successfulConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡Hecho!" message:@"La reseña ha sido actualizada" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Delegate methods

- (void)getNewReview{
    newBook = YES;
    [self getLastReview];
}

@end
