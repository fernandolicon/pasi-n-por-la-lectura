//
//  SearchUserViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUser.h"
#import "PPLUserInfo.h"
#import "PPLOtherUserViewController.h"
#import "PPLAppDelegate.h"

@interface PPLSearchUserViewController : UIViewController <UISearchBarDelegate>{
    UIApplication *networkIdentifier;
    NSMutableDictionary *imageUsers;
    
    PPLUser *selectedUser;
}

@property (nonatomic, strong) NSMutableArray *listUsers;

- (void) successfulSearchofUsers: (NSMutableArray *) foundUsers;
- (void) errorInConnection;

@end
