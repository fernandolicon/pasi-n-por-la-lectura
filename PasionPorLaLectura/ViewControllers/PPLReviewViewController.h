//
//  ReviewViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewMyReviewDelegate

- (void)getNewReview;

@end

#import "TPCRateView.h"
#import "PPLReview.h"
#import "PPLReviewRequest.h"

@interface PPLReviewViewController : UIViewController <UITextViewDelegate, RateViewDelegate>{
    
    NSMutableArray *calif;
    __weak IBOutlet UILabel *lblCharCount;
    
    __weak IBOutlet UITextView *txtResena;
    
    UIApplication *networkIdentifier;
}

- (IBAction)enviarResena:(id)sender;
@property int idReviewBook;
@property (weak, nonatomic) PPLReview *lastReview;
@property (weak, nonatomic) IBOutlet TPCRateView *rateView;

@property (nonatomic) id<NewMyReviewDelegate> delegate;

- (void) successfulConnection;
- (void) errorInConnection;

@end
