//
//  PresentEndorsmentsViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLPresentEndorsementsViewController.h"

@interface PPLPresentEndorsementsViewController ()

@end

@implementation PPLPresentEndorsementsViewController

@synthesize typeEndorsement;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    networkIdentifier = [UIApplication sharedApplication];
    networkIdentifier.networkActivityIndicatorVisible = YES;
    
    refreshEndorsements = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:refreshEndorsements];
    reload = NO;
    [refreshEndorsements addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    //BOOL in order to know if a refresh was performed by the user
    reload = NO;
    
    self.tableView.separatorColor = [UIColor clearColor];
    
    if ([typeEndorsement isEqualToString:@"E"]) {
        self.title = @"Endosados";
    }else{
        self.title = @"Pendientes";
    }
    
    [self performConnection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NetworkConnections

- (void) performConnection{
    [PPLEndorsementsRequest getEndorsmentswithStatus:typeEndorsement forViewController:self];
}

- (void) successfulConnectionwithArray:(NSMutableArray *)endorsed{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    listEndorsements = endorsed;
    if (reload) {
        [refreshEndorsements endRefreshing];
        reload = NO;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        [self.tableView reloadData];
    }
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Reload Data

- (void)refreshTable {
    reload = YES;
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [self performConnection];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listEndorsements.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPLCeldaLibro *cell = [tableView dequeueReusableCellWithIdentifier:@"EndorsementBookCell2"];
    PPLEndorsement *provisionalEndorsment = [listEndorsements objectAtIndex:indexPath.row];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLCeldaLibro" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    cell.nombre.text = provisionalEndorsment.book.name;
    cell.autor.text = [NSString stringWithFormat:@"Alumno:  %@ %@",provisionalEndorsment.user.firstName, provisionalEndorsment.user.lastName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imgLibro.image = nil;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedEndorsement = [listEndorsements objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"SeeEndorsement" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    PPLSeeEndorsementViewController *nextVC = [segue destinationViewController];
    nextVC.endorsement = selectedEndorsement;
    nextVC.delegate = self;
}

#pragma mark - SeeEndorseDelegate

- (void) removeEndorsefromList:(PPLEndorsement *)removeEndorse{
    for (int i = 0; i < listEndorsements.count; i++) {
        if ([removeEndorse isEqualtoEndorse:listEndorsements[i]]) {
            [listEndorsements removeObjectAtIndex:i];
        }
    }
    
    [self.tableView reloadData];
}

@end
