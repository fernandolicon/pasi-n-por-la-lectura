//
//  SettingsViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 02/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLLogInViewController.h"
#import "PPLUser.h"
#import "PPLSeeEndorsementViewController.h"

@interface PPLSettingsViewController : UITableViewController{
    NSArray *items;
    NSArray *itemsPictures;
}

@end
