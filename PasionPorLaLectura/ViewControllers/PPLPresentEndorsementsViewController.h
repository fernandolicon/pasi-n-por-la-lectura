//
//  PresentEndorsmentsViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLSeeEndorsementViewController.h"
#import "PPLEndorsement.h"
#import "PPLEndorsementsRequest.h"
#import "CeldaLibro/PPLCeldaLibro.h"

@interface PPLPresentEndorsementsViewController : UITableViewController <SeeEndorsementDelegate>{
    NSMutableArray *listEndorsements;
    UIApplication *networkIdentifier;
    PPLEndorsement *selectedEndorsement;
    BOOL reload;
    
    UIRefreshControl *refreshEndorsements;
    
}

@property (nonatomic, strong) NSString *typeEndorsement;

- (void) successfulConnectionwithArray: (NSMutableArray *) endorsed;
- (void) errorInConnection;

@end
