//
//  EditarPerfilViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 31/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLEditCitaViewController.h"

@interface PPLEditCitaViewController ()

@end

@implementation PPLEditCitaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.title = @"Editar cita";
    
    if (self.quote != nil) {
        txtCita.text = self.quote;
    }
    
    networkIndicator = [UIApplication sharedApplication];
    
    quoteUpdated = NO;
}

- (void) dismissKeyboard{
    [txtCita resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NetworkConnection

- (IBAction)guardarCita:(id)sender{
    networkIndicator.networkActivityIndicatorVisible = YES;
    NSData *quotetoEncode = [txtCita.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *quoteEncoded = [quotetoEncode base64EncodedStringWithOptions:0];
    [PPLUpdateProfileRequest updateQuote:quoteEncoded forView:self];
}

- (void)successfulUpdate{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡Hecho!" message:@"La cita ha sido actualizada" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [self.delegate getNewQuote:txtCita.text];
    [self.navigationController popViewControllerAnimated:YES];
    quoteUpdated = YES;
}

- (void) errorInConnection{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión. Intente más tarde." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
