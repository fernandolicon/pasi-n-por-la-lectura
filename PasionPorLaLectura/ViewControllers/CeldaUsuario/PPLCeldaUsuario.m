//
//  CeldaUsuario.m
//  pasionporlectura
//
//  Created by ricardo on 12/10/13.
//  Copyright (c) 2013 ITESM Chihuahua. All rights reserved.
//

#import "PPLCeldaUsuario.h"

@implementation PPLCeldaUsuario

@synthesize nombre;
@synthesize img;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
