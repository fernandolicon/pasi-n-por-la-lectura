//
//  CeldaUsuario.h
//  pasionporlectura
//
//  Created by ricardo on 12/10/13.
//  Copyright (c) 2013 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PPLCeldaUsuario : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UIImageView *img;


@end
