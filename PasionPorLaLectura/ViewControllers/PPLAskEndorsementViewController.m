//
//  AskEndorsementViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLAskEndorsementViewController.h"

@interface PPLAskEndorsementViewController ()

@end

@implementation PPLAskEndorsementViewController

@synthesize idBook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
	// Do any additional setup after loading the view.
    
    networkIdentifier = [UIApplication sharedApplication];
    
    if (!listUsers) {
        if (!listUsers) {
            listUsers = [[NSMutableArray alloc] init];
        }
    }
    
    self.searchDisplayController.searchResultsTableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    NSString *encodedString = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [PPLUserInfo searchTeacherwithString:encodedString andView:self];
}

#pragma mark - NetworkConnection

- (void) successfulConnectionwithList: (NSMutableArray *) foundUsers{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    [listUsers removeAllObjects];
    listUsers = foundUsers;
    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (void) successfulAskEndorsement{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Solicitud Enviada" message:@"¡Tu solicitud ha sido enviada!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) repeatedEndorsement{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"El endosado se solicitó con anterioridad" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"someID"];
    PPLUser *provisionalUser = [listUsers objectAtIndex:indexPath.row];
    
    //Use this code for future implementations of Get Image and Celda Usuario
    /*
     if (cell == nil){
     NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CeldaUsuario" owner:self options:nil];
     cell = [nib objectAtIndex:0];
     }
     
     cell.nombre.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
     //NSURL *imgurl = provisionalUser.pictureUrl;
     //NSData *imageData = [NSData dataWithContentsOfURL:imgurl];
     //cell.img.image = [UIImage imageWithData:imageData];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;*/
    
    
    if ( cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"miCuentaCell"];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    teacher = [listUsers objectAtIndex:indexPath.row];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Solicitar endosado" message:@"¿Estas seguro?" delegate:self cancelButtonTitle:@"Si" otherButtonTitles:@"No", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        networkIdentifier.networkActivityIndicatorVisible = YES;
        [PPLEndorsementsRequest askEndorsementToTeacher:teacher.idUser forBook:idBook andView:self];
        
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}


@end
