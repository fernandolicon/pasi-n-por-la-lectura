//
//  AskEndorsementViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUserInfo.h"
#import "PPLEndorsementsRequest.h"

@interface PPLAskEndorsementViewController : UIViewController <UISearchBarDelegate>{
    UIApplication *networkIdentifier;
    NSMutableArray *listUsers;
    
    PPLUser *teacher;
}

@property int idBook;

- (void) successfulConnectionwithList: (NSMutableArray *) foundUsers;
- (void) successfulAskEndorsement;
- (void) repeatedEndorsement;
- (void) errorInConnection;

@end
