//
//  FollowersViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 1/31/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLFollowersViewController.h"

@interface PPLFollowersViewController ()

@end

@implementation PPLFollowersViewController
@synthesize listUsers;
@synthesize titleView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    imageUsers = [[NSMutableDictionary alloc] init];
    networkIndicator = [UIApplication sharedApplication];
    
    networkIndicator.networkActivityIndicatorVisible = YES;
    for (PPLUser *user in listUsers) {
        NSString *key = [NSString stringWithFormat:@"%i", user.idUser];
        NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=65", [PPLUser getUserName], [PPLUser getPassword], user.idUser];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        [imageUsers setObject:imageData forKey:key];
    }
    networkIndicator.networkActivityIndicatorVisible = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.edit
    
    self.title = titleView;
    
    self.tableView.separatorColor = [UIColor clearColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPLCeldaUsuario *cell = [tableView dequeueReusableCellWithIdentifier:@"FollowersCell"];
    PPLUser *provisionalUser = [listUsers objectAtIndex:indexPath.row];
    
    //Use this code for future implementations of Get Image and Celda Usuario
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLCeldaUsuario" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.nombre.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
    //NSURL *imgurl = provisionalUser.pictureUrl;
    //NSData *imageData = [NSData dataWithContentsOfURL:imgurl];
    //cell.img.image = [UIImage imageWithData:imageData];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nombre.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
    NSString *key = [NSString stringWithFormat:@"%i", provisionalUser.idUser];
    cell.img.image = [UIImage imageWithData:[imageUsers objectForKey:key]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedUser = [listUsers objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"SeeUser" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    PPLOtherUserViewController *nextVC = [segue destinationViewController];
    nextVC.otherUser = selectedUser;
}

@end
