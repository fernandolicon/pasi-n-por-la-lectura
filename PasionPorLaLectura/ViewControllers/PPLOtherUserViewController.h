//
//  OtherUserViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 04/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUser.h"
#import "PPLUserInfo.h"
#import "PPLConnectUsersRequest.h"

@interface PPLOtherUserViewController : UIViewController{
    __weak IBOutlet UIImageView *imgPerfil;
    __weak IBOutlet UITextView *txtCita;
    __weak IBOutlet UITextView *txtNombre;
    __weak IBOutlet UILabel *txtPrograma;
    __weak IBOutlet UILabel *txtSeguidores;
    __weak IBOutlet UILabel *txtSiguiendo;
    __weak IBOutlet UILabel *txtLeidos;
    __weak IBOutlet UIBarButtonItem *btnFollow;
    __weak IBOutlet UIScrollView *scrllView;
    
    int connectionsCounter;
    BOOL reload;
    
    UIRefreshControl *refreshUser;
    UIApplication *networkIndicator;
    NSMutableArray *listUsrFollowing;
    NSMutableArray *listUsrFollowers;
    NSMutableArray *listReadBooks;
    NSMutableArray *sentUsers; //Used to storage followers or following depending on what is called
    
    NSString *nextViewTitle;
}

@property (nonatomic, strong) PPLUser *otherUser;

- (void) errorinConnection;
- (void) successfulConnectionforFollowingUsers: (NSMutableArray *) usersFollowing;
- (void) successfulConnectionforFollowdUsers: (NSMutableArray *) usersFollowed;
- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead;
- (void) userIsFollowing;
- (void) correctFollowUser;
- (void) correctUnfollowUser;

- (IBAction)actionForUser:(id)sender;



- (IBAction)viewFollowers:(id)sender;
- (IBAction)viewFollowing:(id)sender;
- (IBAction)viewReadBooks:(id)sender;

@end
