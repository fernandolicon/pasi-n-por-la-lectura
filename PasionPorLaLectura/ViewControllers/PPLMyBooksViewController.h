//
//  MyBooksViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLBooksReqests.h"
#import "PPLBook.h"
#import "PPLCeldaLibro.h"
#import "PPLBookViewController.h"

@interface PPLMyBooksViewController : UITableViewController <NewBookDelegate>{
    NSMutableArray *myBooks;
    
    UIApplication *networkIdentifier;
    UIRefreshControl *refreshInfo;
    UIView *whiteView;
    
    PPLBook *slctdBook;
    
    BOOL reload;
    BOOL emptyBooks;
    
    
}

- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead;
- (void) errorinConnection;
extern BOOL newReadBook;

@end
