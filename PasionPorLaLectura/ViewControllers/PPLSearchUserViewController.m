//
//  SearchUserViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLSearchUserViewController.h"

@interface PPLSearchUserViewController ()

@end

@implementation PPLSearchUserViewController
@synthesize listUsers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [PPLAppDelegate changeStatusBarColorforView:self];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if (!listUsers) {
        if (!listUsers) {
            listUsers = [[NSMutableArray alloc] init];
        }
    }
    
    imageUsers = [[NSMutableDictionary alloc] init];
    
    networkIdentifier = [UIApplication sharedApplication];
    
    self.searchDisplayController.searchResultsTableView.separatorColor = [UIColor clearColor];
}

- (void) viewDidAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NetworkConnections

- (void) successfulSearchofUsers:(NSMutableArray *)foundUsers{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    [listUsers removeAllObjects];
    listUsers = foundUsers;
    for (int i = 0; i < listUsers.count; i++) {
        PPLUser *provisionalUsr = [listUsers objectAtIndex:i];
        if (provisionalUsr.idUser == [PPLUser getIdUser]) {
            [listUsers removeObjectAtIndex:i];
        }
    }
    
    for (PPLUser *user in foundUsers) {
        NSString *key = [NSString stringWithFormat:@"%i", user.idUser];
        NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=65", [PPLUser getUserName], [PPLUser getPassword], user.idUser];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        [imageUsers setObject:imageData forKey:key];
    }
    
    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - NavigationBar

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    NSString *encodedString = [searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [PPLUserInfo searchUserwithString:encodedString andView:self];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPLCeldaUsuario *cell = [tableView dequeueReusableCellWithIdentifier:@"FollowersCell"];
    PPLUser *provisionalUser = [listUsers objectAtIndex:indexPath.row];
    
    //Use this code for future implementations of Get Image and Celda Usuario
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLCeldaUsuario" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.nombre.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nombre.text = [NSString stringWithFormat:@"%@ %@", provisionalUser.firstName, provisionalUser.lastName];
    NSString *key = [NSString stringWithFormat:@"%i", provisionalUser.idUser];
    cell.img.image = [UIImage imageWithData:[imageUsers objectForKey:key]];
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedUser = [listUsers objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"PresentUser" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    PPLOtherUserViewController *nextVC = [segue destinationViewController];
    nextVC.otherUser = selectedUser;
}


@end
