//
//  SettingsViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 02/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLSettingsViewController.h"

@interface PPLSettingsViewController ()

@end

@implementation PPLSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem; if (!items) {
    items = [[NSArray alloc] initWithObjects:@"Desconectar", nil];
    itemsPictures = [[NSArray alloc] initWithObjects:@"logout-7.png", nil];

    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"SettingsCell"];
    
    if (celda == nil){
        celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SettingsCell"];
    }
    
    celda.textLabel.text = [items objectAtIndex:indexPath.row];
    celda.imageView.image = [UIImage imageNamed:[itemsPictures objectAtIndex:indexPath.row]];
    
    
    return celda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Desconectar" message:@"¿Estas seguro?" delegate:self cancelButtonTitle:@"Si" otherButtonTitles:@"No", nil];
            [alert show];
            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [PPLUser resetData];
        UIStoryboard *storyboard;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // The device is an iPad running iOS 3.2 or later.
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            //MasterViewController *pantalla = (MasterViewController *)[storyboard instantiateViewControllerWithIdentifier:@"inicio"];
            //[self presentViewController:pantalla animated:YES completion:nil];
        }
        else
        {
            // The device is an iPhone or iPod touch.
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            PPLLogInViewController *pantalla = (PPLLogInViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LogIn"];
            [self presentViewController:pantalla animated:YES completion:nil];
        }
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
}

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
