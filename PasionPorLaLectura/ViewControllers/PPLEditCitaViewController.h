//
//  EditarPerfilViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 31/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUpdateProfileRequest.h"

@protocol EditCitaDelegate

- (void)getNewQuote:(NSString *) newQuote;

@end

@interface PPLEditCitaViewController : UIViewController{
    
    __weak IBOutlet UITextView *txtCita;
    
    UIApplication *networkIndicator;
    
    BOOL quoteUpdated;
    
}

@property (nonatomic, strong) NSString *quote;

@property (nonatomic) id<EditCitaDelegate> delegate;

- (IBAction)guardarCita:(id)sender;

- (void)successfulUpdate;
- (void) errorInConnection;

@end