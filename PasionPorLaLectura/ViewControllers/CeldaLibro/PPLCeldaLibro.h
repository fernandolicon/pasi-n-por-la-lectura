//
//  CeldaLibro.h
//  pasionporlectura
//
//  Created by ricardo on 12/6/13.
//  Copyright (c) 2013 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPLCeldaLibro : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *autor;
@property (weak, nonatomic) IBOutlet UIImageView *imgLibro;


@end
