//
//  CeldaLibro.m
//  pasionporlectura
//
//  Created by ricardo on 12/6/13.
//  Copyright (c) 2013 ITESM Chihuahua. All rights reserved.
//

#import "PPLCeldaLibro.h"

@implementation PPLCeldaLibro

@synthesize nombre;
@synthesize autor;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
