//
//  SearchBookViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/1/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLSearchBookViewController.h"

@interface PPLSearchBookViewController ()

@end

@implementation PPLSearchBookViewController

@synthesize listBooks;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [PPLAppDelegate changeStatusBarColorforView:self];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if (!listBooks) {
        if (!listBooks) {
            listBooks = [[NSMutableArray alloc] init];
        }
    }
    
    networkIdentifier = [UIApplication sharedApplication];
    
    self.searchDisplayController.searchResultsTableView.separatorColor = [UIColor clearColor];
}

- (void) viewDidAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NetworkConnections

- (void) successfulSearchofBooks: (NSMutableArray *) foundBooks{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    [listBooks removeAllObjects];
    listBooks = foundBooks;
    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - NavigationBar

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    NSString *encodedString = [searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [PPLBooksReqests searchBookwithString:encodedString andView:self];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listBooks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPLCeldaLibro *cell = [tableView dequeueReusableCellWithIdentifier:@"bookCell"];
    PPLBook *provisionalBook = [listBooks objectAtIndex:indexPath.row];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLCeldaLibro" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.nombre.text = provisionalBook.name;
    cell.autor.text = provisionalBook.author;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    slctdBook = [listBooks objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ViewFoundBook" sender:self];
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    PPLBookViewController *nextVC = [segue destinationViewController];
    nextVC.selectedBook = slctdBook;
}

@end
