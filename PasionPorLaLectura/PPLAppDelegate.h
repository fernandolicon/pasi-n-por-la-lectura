//
//  AppDelegate.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLLogIn.h"
#import "PPLLogInViewController.h"

@interface PPLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property BOOL newFollow;

+ (void) changeStatusBarColorforView: (UIViewController *) view;
- (void) errorInConnection;
- (void) errorinData;

@end
