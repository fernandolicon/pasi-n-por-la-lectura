//
//  LogInViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLLogInViewController.h"

@interface PPLLogInViewController ()

@end

@implementation PPLLogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    networkIndicator = [UIApplication sharedApplication];
}

- (void) viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
}

- (void) dismissKeyboard{
    [txtUser resignFirstResponder];
    [txtPsswd resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Connections

- (IBAction)doConnection:(id)sender {
    networkIndicator.networkActivityIndicatorVisible = YES;
    NSString *user = txtUser.text;
    NSString *psswd = txtPsswd.text;
    [PPLLogIn performLogInforUser:user andPassword:psswd forView:self];
}

- (void) sucessfullConnectionforUser:(PPLUser *)activeUser withUserName:(NSString *)userName withPassword:(NSString *)password{
    networkIndicator.networkActivityIndicatorVisible = NO;
    [PPLUser setUserName:userName];
    [PPLUser setPassword:password];
    [PPLUser setIdUser:activeUser.idUser];
    [PPLUser setUserType:activeUser.type];
    [self performSegueWithIdentifier:@"home" sender:self];
}

- (void) errorwithData{
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Datos Incorrectos" message:@"El usuario o contraseña son incorrectos. Por favor intentelo de nuevo." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}


- (void) errorwithConnection{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

@end
