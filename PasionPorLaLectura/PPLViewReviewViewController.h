//
//  PPLViewReviewViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 22/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLReview.h"
#import "ThirdClasses/TPCRateView.h"

@interface PPLViewReviewViewController : UIViewController<RateViewDelegate>{
    
    __weak IBOutlet UILabel *bookName;
    __weak IBOutlet UILabel *author;
    __weak IBOutlet UILabel *userName;
    __weak IBOutlet UILabel *userReview;
    __weak IBOutlet UIImageView *userImage;
}

@property (nonatomic, copy) NSString *nameBook;
@property (nonatomic, copy) NSString *authorBook;
@property (nonatomic, strong) PPLReview *review;
@property (nonatomic, strong) NSData *imageUser;
@property (weak, nonatomic) IBOutlet TPCRateView *rateView;

@end
