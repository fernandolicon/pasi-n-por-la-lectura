//
//  PPLAllReviewsTableViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 21/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLAllReviewsTableViewController.h"
#import "PPLViewReviewViewController.h"

@interface PPLAllReviewsTableViewController ()

@end

@implementation PPLAllReviewsTableViewController

@synthesize idBook;

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    isLastReview = NO;
    
    networkIdentifier = [UIApplication sharedApplication];
    networkIdentifier.networkActivityIndicatorVisible = YES;
    
    self.title = @"Reseñas";
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    reviews = [[NSMutableArray alloc] init];
    
    imagesReview = [[NSMutableDictionary alloc] init];
    
    emptyFeed = NO;
    
    counterofReviews = 0;
    selectedRow = 0;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self performConnection];
}

- (void) viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (reviews.count == 0) {
        emptyFeed = YES;
        return 1;
    }else{
        emptyFeed = NO;
        if (isLastReview) {
            return reviews.count;
        }else{
            return reviews.count + 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == reviews.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReviewCell"];
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ReviewCell"];
        }
        
        cell.textLabel.text = @"Cargar más reseñas...";
        return cell;
    }
    
    PPLLastReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LastReviewCell"];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PPLLastReviewTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PPLReview *provisionalReview = [reviews objectAtIndex:indexPath.row];
    
    cell.userName.text = [NSString stringWithFormat:@"%@ %@", provisionalReview.user.firstName, provisionalReview.user.lastName];
    cell.userReview.text = provisionalReview.reviewText;
    
    [cell.userReview setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
    cell.rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
    cell.rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    cell.rateView.rating = 0;
    cell.rateView.maxRating = 5;
    cell.rateView.delegate = cell;
    cell.rateView.editable = NO;
    
    cell.rateView.rating = provisionalReview.rate;
    
    NSString *key = [NSString stringWithFormat:@"%i", provisionalReview.user.idUser];
    cell.imgUser.image = [UIImage imageWithData:[imagesReview objectForKey:key]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == reviews.count) {
        return 44;
    }
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == reviews.count) {
        networkIdentifier.networkActivityIndicatorVisible = YES;
        [self performConnection];
    }else{
        selectedRow = (int)indexPath.row;
        [self performSegueWithIdentifier:@"ViewReview" sender:self];
    }
}

#pragma mark - NetworkConnections

- (void) performConnection{
    [PPLReviewRequest getAllLastReviewsforBook:idBook fromReviewNumber:counterofReviews numberofReviews:5 forView:self];
}

- (void) successfulConnectionwithArray:(NSMutableArray *) lastReviews andTotalReviews:(int)totalReviews{
    [reviews addObjectsFromArray:lastReviews];
    
    //This for downloads images for the feed in order to improve performance
    while (counterofReviews < reviews.count) {
        PPLReview *review = [reviews objectAtIndex:counterofReviews];
        NSString *key = [NSString stringWithFormat:@"%i", review.user.idUser];
        NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=70", [PPLUser getUserName], [PPLUser getPassword], review.user.idUser];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        [imagesReview setObject:imageData forKey:key];
        counterofReviews++;
    }
    
    if (totalReviews <= counterofReviews) {
        isLastReview = YES;
    }
    
    [self.tableView reloadData];
    
    networkIdentifier.networkActivityIndicatorVisible = NO;
}

- (void) errorInConnection{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de Conexión" message:@"Hubo un error de conexión al acceder al sistema" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ViewReview"]) {
        PPLViewReviewViewController *nextVC = [segue destinationViewController];
        PPLReview *nextReview = [reviews objectAtIndex:selectedRow];
        NSString *key = [NSString stringWithFormat:@"%i", nextReview.user.idUser];
        nextVC.imageUser = [imagesReview objectForKey:key];
        nextVC.nameBook = self.nameBook;
        nextVC.authorBook = self.author;
        nextVC.review = nextReview;
    }
}

@end
