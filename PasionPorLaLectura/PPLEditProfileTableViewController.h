//
//  EditProfileTableViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLEditProfilePhotoTableViewCell.h"
#import "PPLEditCitaViewController.h"
#import "PECropViewController.h"

@protocol EditPerfilDelegate

- (void) writeNewQuote:(NSString *) newQuote;
- (void) changeProfilePicture: (UIImage *) newImage;

@end

@interface PPLEditProfileTableViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, EditCitaDelegate, PECropViewControllerDelegate, UIActionSheetDelegate>{
    UIActionSheet *editPhotoPopUp;
    UIPopoverController *popController;
    UIApplication *networkIndicator;
    UIImage *imageSelected;
    UIImage *imageCropped;
    
    BOOL pictureUpdated;
    BOOL quoteUpdated;
}

@property (nonatomic, strong) UIImage *imgUser;
@property (nonatomic, strong) NSString *quote;

@property (nonatomic) id<EditPerfilDelegate> delegate;

- (void)successfulUpdate;
- (void) errorInConnectionforPicture;

@end
