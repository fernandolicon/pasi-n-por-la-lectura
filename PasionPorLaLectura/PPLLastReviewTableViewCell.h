//
//  PPLLastReviewTableViewCell.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 21/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThirdClasses/TPCRateView.h"

@interface PPLLastReviewTableViewCell : UITableViewCell<RateViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userReview;
@property (weak, nonatomic) IBOutlet TPCRateView *rateView;

@end
