//
//  Book.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPLBook : NSObject

@property int idBook;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *edition;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *subject;
@property (nonatomic, copy) NSString *editor;
@property (nonatomic, copy) NSString *ISBN;
@property int year;

- (instancetype) initWithDictionary: (NSDictionary *) bookDictionary;

@end
