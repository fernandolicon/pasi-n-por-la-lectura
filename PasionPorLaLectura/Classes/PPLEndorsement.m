//
//  Endorsment.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLEndorsement.h"

@implementation PPLEndorsement

- (instancetype) initWithDictionary: (NSDictionary *) endorsementDictionary{
    if (self = [super init]){
        self.book = [[PPLBook alloc] initWithDictionary:[endorsementDictionary objectForKey:@"book"]];
        self.user = [[PPLUser alloc] initWithDictionary:[endorsementDictionary objectForKey:@"user"]];
        self.status = [endorsementDictionary objectForKey:@"state"];
        
    }
    return self;
}

- (BOOL) isEqualtoEndorse: (PPLEndorsement *) toCompare{
    if ((self.user.idUser == toCompare.user.idUser)&& (self.book.idBook == toCompare.book.idBook)) {
        return YES;
    }
    return NO;
}

@end
