//
//  Endorsment.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLUser.h"
#import "PPLBook.h"

@interface PPLEndorsement : NSObject

@property (nonatomic, strong) PPLUser *user;
@property (nonatomic, strong) PPLBook *book;
@property (nonatomic, copy) NSString *status;

- (instancetype) initWithDictionary: (NSDictionary *) endorsementDictionary;
- (BOOL) isEqualtoEndorse: (PPLEndorsement *) toCompare;

@end
