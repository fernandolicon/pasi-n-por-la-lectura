//
//  User.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLUser.h"

@implementation PPLUser

- (instancetype) initWithDictionary: (NSDictionary *) userDictionary{
    if(self = [super init]){
            self.userName = [userDictionary objectForKey:@"user_name"];
            self.idUser = [[userDictionary objectForKey:@"id_user"] intValue];
            self.pictureUrl = [NSURL URLWithString:[userDictionary objectForKey:@"picture_url"]];
            self.userKey = [userDictionary objectForKey:@"user_key"];
            self.program = [userDictionary objectForKey:@"program"];
            self.quote = [userDictionary objectForKey:@"quote"];
            self.firstName = [userDictionary objectForKey:@"name"];
            self.lastName = [userDictionary objectForKey:@"last_name"];
            self.type = [userDictionary objectForKey:@"type"];
            self.email = [userDictionary objectForKey:@"user_email"];
    }
    
    return self;
}

+ (NSString *)getUserName{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    if (u == nil)
        u = @"";
    return u;
}

+ (void) setUserName: (NSString *) username{
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"user_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) getPassword{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    if (u == nil)
        u = @"";
    return u;
}

+ (void) setPassword: (NSString *) password{
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (int) getIdUser{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"];
    int idUser = 0;
    if (u != nil){
        idUser = [u intValue];
    }
    
    return idUser;
}

+ (void) setIdUser:(int)idUser{
    NSString *idString = [NSString stringWithFormat:@"%i", idUser];
    [[NSUserDefaults standardUserDefaults] setObject:idString forKey:@"idUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) getUserType{
    NSString *u = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"userType"];
    if (u == nil){
        u = @"";
    }
    
    return u;
}

+ (void) setUserType:(NSString *)userType{
    [[NSUserDefaults standardUserDefaults] setObject:userType forKey:@"userType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) resetData{
    [self setUserName:@""];
    [self setPassword:@""];
    [self setIdUser:0];
    [self setUserType:@""];
}

@end
