//
//  User.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPLUser : NSObject

@property (nonatomic, strong) NSString *userName;
@property int idUser;
@property (nonatomic, strong) NSURL *pictureUrl;
@property (nonatomic, copy) NSString *userKey;
@property (nonatomic, copy) NSString *program;
@property (nonatomic, copy) NSString *quote;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *email;

+ (NSString *) getUserName;
+ (NSString *) getPassword;
+ (int) getIdUser;
+ (NSString *) getUserType;
+ (void) setUserName: (NSString *) username;
+ (void) setPassword: (NSString *) password;
+ (void) setIdUser: (int) idUser;
+ (void) setUserType: (NSString *) userType;
+ (void) resetData;

- (instancetype) initWithDictionary: (NSDictionary *) userDictionary;

@end
