//
//  Review.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPLUser.h"

@interface PPLReview : NSObject

@property int rate;
@property (nonatomic, copy) NSString *reviewText;
@property (nonatomic, strong) PPLUser *user;

- (instancetype) initWithDictionary: (NSDictionary *) reviewDictionary;

@end
