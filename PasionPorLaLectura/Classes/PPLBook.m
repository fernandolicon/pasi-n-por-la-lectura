//
//  Book.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLBook.h"

@implementation PPLBook

- (instancetype) initWithDictionary: (NSDictionary *) bookDictionary{
    if (self = [super init]){
        self.edition = [bookDictionary objectForKey:@"edition"];
        self.author = [bookDictionary objectForKey:@"author"];
        self.subject = [bookDictionary objectForKey:@"subject"];
        self.editor = [bookDictionary objectForKey:@"editor"];
        self.name = [bookDictionary objectForKey:@"name"];
        self.ISBN = [bookDictionary objectForKey:@"isbn"];
        self.year = [[bookDictionary objectForKey:@"year"] intValue];
        self.idBook = [[bookDictionary objectForKey:@"id_book"] intValue];
    }
    
    return self;
}

@end
