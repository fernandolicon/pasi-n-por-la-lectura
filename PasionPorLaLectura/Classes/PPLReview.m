//
//  Review.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLReview.h"

@implementation PPLReview

- (instancetype) initWithDictionary: (NSDictionary *) reviewDictionary{
    if (self = [super init]) {
        self.user = [[PPLUser alloc] initWithDictionary:[reviewDictionary objectForKey:@"user"]];
        self.rate = [[reviewDictionary objectForKey:@"rate"] intValue];
        self.reviewText = [reviewDictionary objectForKey:@"review"];
    }
    
    return self;
}

@end
