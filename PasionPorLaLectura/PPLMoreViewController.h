//
//  MoreViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/2/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLRecommendedBooksViewController.h"
#import "PPLUser.h"

@interface PPLMoreViewController : UITableViewController{
    NSArray *items;
    NSArray *itemsImgs;
    char type;
}

@end
