//
//  LogInViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 26/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLUser.h"
#import "PPLLogIn.h"

@interface PPLLogInViewController : UIViewController<UITextFieldDelegate>{
    
    __weak IBOutlet UITextField *txtUser;
    __weak IBOutlet UITextField *txtPsswd;
    UIApplication *networkIndicator;
}

- (IBAction)doConnection:(id)sender;
- (void) sucessfullConnectionforUser: (PPLUser *) activeUser withUserName: (NSString *) userName withPassword: (NSString *) password;
- (void) errorwithData;
- (void) errorwithConnection;


@end
