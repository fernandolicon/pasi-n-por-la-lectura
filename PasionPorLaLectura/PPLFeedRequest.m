//
//  PPLFeedRequest.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLFeedRequest.h"

@implementation PPLFeedRequest

+ (void) getFeedforViewController:(PPLFeedViewController *)controller{
    NSString *user = [PPLUser getUserName];
    NSString *psswd = [PPLUser getPassword];
    NSMutableArray *listFeeds = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@User/getFeed?pattern=%@&pwd=%@&dias=30", pasionWSBaseUrl, user, psswd];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *diccProvisional = [responseObject objectForKey:@"response"];
        NSMutableArray *namesObjects = [diccProvisional objectForKey:@"objs"];
 
        for (int i = 0; i < namesObjects.count; i++) {
            NSString *numberofEndorse = namesObjects[i];
            PPLFeed *provisionalFeed = [[PPLFeed alloc] initWithDictionary:[diccProvisional objectForKey:numberofEndorse]];
            [listFeeds addObject:provisionalFeed];
        }
        [controller successfulConnectionwithArray:listFeeds];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [controller errorInConnection];
    }];
    
}

@end
