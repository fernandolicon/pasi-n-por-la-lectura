//
//  MoreViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 2/2/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLMoreViewController.h"

@interface PPLMoreViewController ()

@end

@implementation PPLMoreViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.tableView.separatorColor = [UIColor clearColor];
    
    
    if (!items) {
        type = [[PPLUser getUserType] characterAtIndex:0];
        switch (type) {
            case 'F':
                items = [[NSArray alloc] initWithObjects:@"Recomendados", @"Endosos", @"Ajustes", nil];
                itemsImgs = [[NSArray alloc] initWithObjects:@"thumb-up-7.png", @"book-7.png", @"spanner-screwdriver-7.png", nil];
                break;
            default:
                items = [[NSArray alloc] initWithObjects:@"Recomendados", @"Ajustes", nil];
                itemsImgs = [[NSArray alloc] initWithObjects:@"thumb-up-7.png", @"spanner-screwdriver-7.png", nil];
                break;
        }
    }
    
    self.tableView.separatorColor = [UIColor clearColor];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"MoreCell"];
    
    if (celda == nil){
        celda = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MoreCell"];
    }
    
    celda.textLabel.text = [items objectAtIndex:indexPath.row];
    celda.imageView.image = [UIImage imageNamed:[itemsImgs objectAtIndex:indexPath.row]];
    
    return celda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            [self performSegueWithIdentifier:@"Recommended" sender:self];
            break;
            
        }
        case 1:
            if (type == 'F') {
                [self performSegueWithIdentifier:@"Endorsments" sender:self];
                break;
            }
        case 2:
        {
            [self performSegueWithIdentifier:@"Settings" sender:self];
            break;
        }
            
    }
}


@end
