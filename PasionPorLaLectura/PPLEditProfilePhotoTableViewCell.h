//
//  EditProfilePhotoTableViewCell.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPLEditProfilePhotoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@end
