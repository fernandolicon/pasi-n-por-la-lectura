//
//  PPLAllReviewsTableViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 21/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLReview.h"
#import "PPLReviewRequest.h"
#import "PPLLastReviewTableViewCell.h"

@interface PPLAllReviewsTableViewController : UITableViewController{
    NSMutableArray *reviews;
    UIApplication *networkIdentifier;
    BOOL isLastReview;
    int counterofReviews;
    int selectedRow;
    
    NSMutableDictionary *imagesReview;
    
    BOOL emptyFeed;
}

@property int idBook;
@property (nonatomic, copy) NSString *nameBook;
@property (nonatomic, copy) NSString *author;

- (void) errorInConnection;
- (void) successfulConnectionwithArray: (NSMutableArray *) lastReviews andTotalReviews: (int) totalReviews;

@end
