//
//  EndorsmentsViewController.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 03/02/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLPresentEndorsementsViewController.h"

@interface PPLEndorsementsViewController : UITableViewController{
    NSArray *items;
    NSString *typeSelected;
}

@end
