//
//  PPLViewReviewViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 22/6/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLViewReviewViewController.h"

@interface PPLViewReviewViewController ()

@end

@implementation PPLViewReviewViewController

@synthesize nameBook;
@synthesize authorBook;
@synthesize imageUser;
@synthesize review;
@synthesize rateView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.title = @"Reseña";
    
    bookName.text = nameBook;
    author.text = authorBook;
    userImage.image = [UIImage imageWithData:imageUser];
    userReview.text = review.reviewText;
    userName.text = [NSString stringWithFormat:@"Reseña de %@:", review.user.firstName];
    
    rateView.notSelectedImage = [UIImage imageNamed:@"star_empty.png"];
    rateView.halfSelectedImage = [UIImage imageNamed:@"star_half.png"];
    rateView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    rateView.rating = 0;
    rateView.maxRating = 5;
    rateView.delegate = self;
    rateView.editable = NO;
    
    rateView.rating = review.rate;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rateView:(TPCRateView *)rateView ratingDidChange:(float)rating{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
