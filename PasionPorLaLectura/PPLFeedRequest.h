//
//  PPLFeedRequest.h
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "PPLUser.h"
#import "PPLFeed.h"
#import "Settings.h"
#import "PPLFeedViewController.h"
@class PPLFeedViewController;

@interface PPLFeedRequest : NSObject

+ (void) getFeedforViewController:(PPLFeedViewController *)controller;

@end
