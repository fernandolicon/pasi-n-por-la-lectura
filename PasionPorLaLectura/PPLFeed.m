//
//  PPLFeed.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 28/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLFeed.h"

@implementation PPLFeed

- (instancetype) initWithDictionary:(NSDictionary *)feedDictionary{
    if (self = [super init]){
        self.bookFeed = [[PPLBook alloc] initWithDictionary:[feedDictionary objectForKey:@"book"]];
        self.userFeed = [[PPLUser alloc] initWithDictionary:[feedDictionary objectForKey:@"user"]];
        self.rate = [[feedDictionary objectForKey:@"rate"] intValue];
        self.reviewText = [feedDictionary objectForKey:@"review"];
    }
    return self;
}

@end
