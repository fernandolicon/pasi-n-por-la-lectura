//
//  ProfileActiveUserViewController.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 27/01/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLProfileActiveUserViewController.h"

@interface PPLProfileActiveUserViewController ()

@end

@implementation PPLProfileActiveUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
	// Do any additional setup after loading the view.
    
    networkIndicator = [UIApplication sharedApplication];
    
    refreshUser = [[UIRefreshControl alloc] init];
    [scrllView addSubview:refreshUser];
    reload = NO;
    [refreshUser addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    
    connectionsCounter = 0;
    
    [self performConnection];
}

-(void) viewDidAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"EditarPerfil"]) {
        PPLEditProfileTableViewController *editProfile = [segue destinationViewController];
        editProfile.quote = activeUsr.quote;
        editProfile.delegate = self;
        editProfile.imgUser = imgPerfil.image;
    }
    if ([segue.identifier isEqualToString:@"ShowFollows"]) {
        PPLFollowersViewController *followView = [segue destinationViewController];
        followView.listUsers = sentUsers;
        followView.titleView = nextViewTitle;
    }
    if ([segue.identifier isEqualToString:@"PresentReadBooks"]) {
        PPLListBooksViewController *followView = [segue destinationViewController];
        followView.listBooks = listReadBooks;
    }
    
}

#pragma -mark NetworkConnection

//In order to perform the update of the view when all the info from the requests has arrived
//we have a counter of connections, when this counter is equal to 3 it means we are in the last
//request, so now we can update the view with the received data.

- (void) performConnection{
    networkIndicator.networkActivityIndicatorVisible = YES;
    [PPLUserInfo getInfoforActiveUserforView:self];
}

- (void) successfulConnectionforUser: (PPLUser *) activeUser{
    activeUsr = activeUser;
    
    if (connectionsCounter == 3){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
    
}

- (void) successfulConnectionforFollowingUsers: (NSMutableArray *) usersFollowing{
    listUsrFollowing = usersFollowing;
    if (connectionsCounter == 3){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) successfulConnectionforFollowdUsers: (NSMutableArray *) usersFollowed{
    listUsrFollowers = usersFollowed;
    if (connectionsCounter == 3){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) successfulConnectionforBooks:(NSMutableArray *)booksRead{
    listReadBooks = booksRead;
    if (connectionsCounter == 3){
        [self allConnectionsPerformed];
    }else{
        connectionsCounter++;
    }
}

- (void) allConnectionsPerformed{
    networkIndicator.networkActivityIndicatorVisible = NO;
    txtNombre.text = [NSString stringWithFormat:@"%@ %@", activeUsr.firstName, activeUsr.lastName];
    txtCita.text = activeUsr.quote;
    
    txtPrograma.text = activeUsr.program;
    [txtNombre setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [txtCita setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    
    NSString *conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listUsrFollowing.count ];
    txtSeguidores.text = conteo;
    
    conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listUsrFollowers.count ];
    txtSiguiendo.text = conteo;
    
    conteo = [NSString stringWithFormat:@" %lu", (unsigned long)listReadBooks.count ];
    txtLeidos.text = conteo;
    
    NSString *urlString = [NSString stringWithFormat:@"http://cml.itesm.mx:8080/Passion/getThumbnail?pattern=%@&pwd=%@&photo_from_user_id=%i&width=115", [PPLUser getUserName], [PPLUser getPassword], [PPLUser getIdUser]];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    imgPerfil.image = [UIImage imageWithData:imageData];
    
    if (reload) {
        [refreshUser endRefreshing];
        reload = NO;
    }
    
    
}

- (void) errorinConnection{
    if (connectionsCounter == 0){
    networkIndicator.networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"Hubo un error de conexión. Intente más tarde." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        connectionsCounter ++;
    }
}

#pragma mark - Reload Data

- (void)refreshView {
    reload = YES;
    networkIndicator.networkActivityIndicatorVisible = YES;
    connectionsCounter = 0;
    [self performConnection];
}

#pragma -mark Buttons Actions


- (IBAction)editProfile:(id)sender {
    [self performSegueWithIdentifier:@"EditarPerfil" sender:sender];
}

- (IBAction)viewFollowers:(id)sender {
    nextViewTitle = @"Seguidores";
    sentUsers = listUsrFollowing;
    [self performSegueWithIdentifier:@"ShowFollows" sender:sender];
}

- (IBAction)viewFollowing:(id)sender {
    nextViewTitle = @"Siguiendo";
    sentUsers = listUsrFollowers;
    [self performSegueWithIdentifier:@"ShowFollows" sender:sender];
}

- (IBAction)viewReadBooks:(id)sender {
    [self performSegueWithIdentifier:@"PresentReadBooks" sender:sender];
}

#pragma -mark ReturningView

- (void) writeNewQuote:(NSString *)newQuote{
    txtCita.text = newQuote;
    [txtCita setFont:[UIFont fontWithName:@"Helvetica" size:20]];
}

- (void) changeProfilePicture:(UIImage *)newImage{
    imgPerfil.image = newImage;
}

@end
