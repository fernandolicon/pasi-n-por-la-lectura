//
//  EditProfilePhotoTableViewCell.m
//  PasionPorLaLectura
//
//  Created by Luis Fernando Mata Licón on 30/05/14.
//  Copyright (c) 2014 ITESM Chihuahua. All rights reserved.
//

#import "PPLEditProfilePhotoTableViewCell.h"

@implementation PPLEditProfilePhotoTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
